﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Report_DMTReport : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ToString());
    SqlTransactionDom STDom = new SqlTransactionDom();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["UID"] == null && string.IsNullOrEmpty(Session["UID"].ToString()))
            {
                Response.Redirect("~/Login.aspx");
            }
            else
            {
                CheckEmptyValue();
            }
        }
    }
    protected void Search_Click(object sender, EventArgs e)
    {
        DataSet dsDMT = CheckEmptyValue();
        dmt_grdview.DataSource = dsDMT;
        dmt_grdview.DataBind();
    }

    protected void BTNExport_Click(object sender, EventArgs e)
    {
        DataSet dsDMT = CheckEmptyValue();
        STDom.ExportData(dsDMT);
    }

    public DataSet CheckEmptyValue()
    {
        DataSet dsDMT = new DataSet();

        try
        {
            string FromDate;
            string ToDate;
            if (String.IsNullOrEmpty(Request["From"]))
            {
                FromDate = "";
            }
            else
            {
                string[] DateSplit = Request["From"].ToString().Split('-');
                FromDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
                FromDate = FromDate + " " + "12:00:00 AM";
            }

            if (String.IsNullOrEmpty(Request["To"]))
            {
                ToDate = "";
            }
            else
            {
                string[] DateSplit = Request["To"].ToString().Split('-');
                ToDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
                ToDate = ToDate + " " + "11:59:59 PM";
            }
            string AgentID = String.IsNullOrEmpty(Request["hidtxtAgencyName"]) | Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"];
            string Tid = txt_TID.Text;

            dsDMT = STDom.GetDMTTransDetail(FromDate, ToDate, Session["UID"].ToString(), Tid);

        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return dsDMT;
    }
}