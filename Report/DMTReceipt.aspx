﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DMTReceipt.aspx.cs" Inherits="Report_DMTReceipt" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <style>
        .main {
            margin-left: 20% !important;
        }

        .heading {
            text-align: center;
        }

        .sendmail {
            float: right;
            color: #fff;
            background-color: #ff434f;
            border: 1px solid #ff434f;
            border-radius: 0.2rem;
            padding: 2px;
        }

        @media print {
            .main {
                margin-left: 0px !important;
            }

            .heading {
                text-align: center !important;
            }

            .sendmail {
                display: none !important;
            }
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">

        <%=HtmlContent %>
        <%--<div style="border: 1px solid #ffc000; width: 800px; border-collapse: initial;" class="main">
            <div class="col-sm-12">
                <a href="#">
                    <img src="https://tripsking.in/Advance_CSS/Icons/logo(ft).png" style="max-width: 200px;"></a><span style="float: right!important; text-align: right; padding: 7px;"><span style="font-size: 17px;">DELIGHT ENTERPRISES</span><br>
                        <span style="font-size: 15px;">MALOOKPUR BAZARIYA,<br>
                            Bareilly, Uttar Pradesh, India, 243001</span><br>
                        <span style="font-size: 15px;">Mobile: 7017067227</span><br>
                        <span style="font-size: 15px;">Email: delightenterprises88@gmail.com</span></span>
            </div>
            <div class="col-sm-12">
                <br>
                <br>
                <hr style="text-align: center; border: 1px solid #ffc000; background-color: #ff414d; margin-top: 0px; margin-bottom: 0;">
                <h4 class="heading">Customer Transaction Receipt 
        <div class="sendmail form-validation">
             <input type="text" placeholder="enter email id" id="ReceiptSendMail" style="width: 200px;">
             <span style="cursor: pointer;" id="btnMailSend">Send Mail</span>
                    </div>
                </h4>
                <table data-toggle="table" style="width: 800px; border-collapse: initial; border: 1px solid #ffc000; font-family: Verdana,Geneva,sans-serif; font-size: 12px; border-spacing: 0px; padding: 0px;">
                    <tbody>

                        <tr style="border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;">
                            <th colspan="2" style="text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;">Beneficiary Name</th>
                            <td colspan="3" style="text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;">MR  MAINUL  HOQUE</td>
                        </tr>
                        <tr style="border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;">
                            <th colspan="2" style="text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;">Beneficiary Account Number</th>
                            <td colspan="3" style="text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;">32737775496</td>
                        </tr>
                        <tr style="border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;">
                            <th colspan="2" style="text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;">Beneficiary Bank's IFSC</th>
                            <td colspan="3" style="text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;">SBIN0004343</td>
                        </tr>
                        <tr style="border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;">
                            <th colspan="2" style="text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;">Bank</th>
                            <td colspan="3" style="text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;">STATE BANK OF INDIA</td>
                        </tr>
                        <tr style="border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;">
                            <th colspan="2" style="text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;">Transaction Mode</th>
                            <td colspan="3" style="text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;">IMPS</td>
                        </tr>
                        <tr style="border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;">
                            <th colspan="2" style="text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;">Transaction ID</th>
                            <td colspan="3" style="text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;">0714F50647</td>
                        </tr>
                        <tr style="border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;">
                            <th colspan="2" style="text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;">Transaction Amount</th>
                            <td colspan="3" style="text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;">₹ 2000</td>
                        </tr>
                        <tr style="border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;">
                            <th colspan="2" style="text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;">Transaction Date &amp; Times</th>
                            <td colspan="3" style="text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;">17 Apr 2021 06:27 PM</td>
                        </tr>
                        <tr style="border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;">
                            <th colspan="2" style="text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;">Transaction Status</th>
                            <td colspan="3" style="text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;">TRANSACTION SUCCESSFUL</td>
                        </tr>
                    </tbody>
                </table>
                <br />
                <br />
                <table data-toggle="table" style="width: 800px; border-collapse: initial; font-family: Verdana,Geneva,sans-serif; font-size: 12px; border-spacing: 0px; padding: 0px;">
                    <tbody>
                        <tr>
                            <td>
                                <p>Date..............................</p>
                            </td>
                            <td>
                                <p style="float: right!important;">Signature Of Customer's......................................................</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <br>
            </div>
        </div>--%>


        <%--<script src="custom/js/common.js"></script>--%>
        <script>
            $(window).on('load', function () { window.print(); });

            <%--$(document.body).on('click', "#btnMailSend", function (e) {
                if (CheckFocusBlankValidation("ReceiptSendMail")) return !1;
                if (PrintCheckEmailValidatoin("ReceiptSendMail")) return !1;

                var emailid = $("#ReceiptSendMail").val();
                $("#btnMailSend").html("Sending... <i class='fa fa-pulse fa-spinner'></i>");
                if (emailid != "") {
                    $.ajax({
                        type: "Post",
                        contentType: "application/json; charset=utf-8",
                        url: "/dmt-manager/PrintTrans.aspx/SendReceiptInMail",
                        data: '{emailid: ' + JSON.stringify(emailid) + '}',
                        datatype: "json",
                        success: function (data) {
                            if (data.d != null) {
                                if (data.d == "sent") {
                                    alert("Transaction receipt has been sent to ' " + emailid + " ' successfully.");
                                    $("#ReceiptSendMail").val("");
                                }
                                else {
                                    alert("Transaction receipt send failed!");
                                }
                            }
                            $("#btnMailSend").html("Send Mail");
                        },
                        failure: function (response) {
                            alert("failed");
                        }
                    });
                }
            });--%>
        
        </script>
    </form>
</body>
</html>
