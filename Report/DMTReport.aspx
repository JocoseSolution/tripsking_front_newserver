﻿<%@ Page Language="C#" AutoEventWireup="false" CodeFile="DMTReport.aspx.cs" Inherits="Report_DMTReport" MasterPageFile="~/MasterPageForDash.master" Title="" %>

<%@ Register Src="~/UserControl/LeftMenu.ascx" TagPrefix="uc1" TagName="LeftMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <link type="text/css" href="<%=ResolveUrl("~/CSS/newcss/main.css")%>" rel="stylesheet" />
    
    <div class="card-main">
        <div class="card-header">
            <h3 class="main-heading">DMT Report</h3>
        </div>
        <div class="card-body report">
            <div class="inner-box">

                <div class="row">
                    <div class="col-md-3">

                        <input type="text" name="From" id="From" placeholder="From Date" class="form-control" readonly="readonly" />
                    </div>
                    <div class="col-md-3">

                        <input type="text" name="To" placeholder="To" id="To" class="form-control" readonly="readonly" />
                    </div>

                    <div class="col-md-3">
                        <asp:TextBox ID="txt_TID" placeholder="TID" class="form-control" runat="server"></asp:TextBox>

                    </div>
                    <div class="col-md-3">
                        <asp:Button ID="btn_result" runat="server" class="btn cmn-btn" OnClick="Search_Click" Text="Search Result" />
                        <asp:Button ID="btn_export" runat="server" class="btn cmn-btn" OnClick="BTNExport_Click" Text="Export" />
                    </div>

                </div>
                <%--<div class="row">
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="txtAgencyName" placeholder="Agency Name or ID" name="txtAgencyName" onfocus="focusObj(this);"
                            onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" />
                        <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                    </div>
                    <div class="col-md-2">
                        
                    </div>
                    <div class="col-md-2">
                        

                    </div>
                </div>--%>

                <hr />

                <%-- <div class="row ticket-bal">

                    <div class="col-md-3">
                        <span>Total Ticket Sale :</span>
                        <span id="ctl00_ContentPlaceHolder1_lbl_Total">0</span>
                    </div>
                    <div class="col-md-3">
                        <span>Total Ticket Issued :</span>
                        <span id="ctl00_ContentPlaceHolder1_lbl_counttkt">0</span>
                    </div>
                </div>--%>
            </div>
        </div>
    </div>

    <div id="divReport" runat="server" class="large-12 medium-12 small-12">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="dmt_grdview" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="False" CssClass="rtable" GridLines="None" Font-Size="12px" PageSize="30">
                    <Columns>
                        <%--<asp:TemplateField HeaderText="AgencyName">
                            <ItemTemplate>
                                <asp:Label ID="agencyname" runat="server" Text='<%#Eval("agencyname")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Recipient Name">
                            <ItemTemplate>
                                <asp:Label ID="Recipient_Name" runat="server" Text='<%#Eval("Recipient_Name")%>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Account No">
                            <ItemTemplate>
                                <asp:Label ID="accountno" runat="server" Text='<%#Eval("accountno")%>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Bank Name">
                            <ItemTemplate>
                                <asp:Label ID="Bank" runat="server" Text='<%#Eval("Bank")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Mode">
                            <ItemTemplate>
                                <asp:Label ID="Channel" runat="server" Text='<%#Eval("Channel")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount">
                            <ItemTemplate>
                                <asp:Label ID="Amount_Transferd" runat="server" Text='<%#Eval("Amount_Transferd")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Trans. Id">
                            <ItemTemplate>
                                <asp:Label ID="TID" runat="server" Text='<%#Eval("TID")%>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Label ID="Response_Status" runat="server" Text='<%#Eval("Response_Status")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date">
                            <ItemTemplate>
                                <asp:Label ID="UpdateOn" runat="server" Text='<%#Eval("UpdateOn")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <a href="/Report/DMTReceipt.aspx?tid=<%#Eval("TID")%>&userid=<%#Eval("userid")%>" target="_blank" class="btn cmn-btn">Print</a>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
                <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                </div>
                <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                    Please Wait....<br />
                    <br />
                    <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                    <br />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <%--</div>--%>
    </div>

  <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Distributor.js") %>"></script>
</asp:Content>
