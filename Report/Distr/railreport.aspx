﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageForDash.master" AutoEventWireup="false" CodeFile="railreport.aspx.vb" Inherits="Report_Distr_railreport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <link type="text/css" href="<%=ResolveUrl("~/CSS/newcss/main.css")%>" rel="stylesheet" />
    <style>
        .rtable {
            width: 100% !important;
        }
    </style>
    <br />
    <div class="card-main">
        <div class="card-header">
            <h3 class="main-heading">Rail Report Details</h3>
        </div>
        <div class="inner-box">
            <div class="row">
                <div class="col-md-3">
                    <label>From Date</label>
                    <%--<asp:TextBox ID="txt_bankname" placeholder="Enter From Date" CssClass="form-control" runat="server"></asp:TextBox>--%>
                    <input type="text" placeholder="FROM DATE" name="From" id="From" readonly="readonly" class="form-control" />
                </div>
                <div class="col-md-3">
                    <label>To Date</label>
                    <%--<asp:TextBox ID="txt_branchname" placeholder="Enter To Date" CssClass="form-control" runat="server"></asp:TextBox>--%>
                    <input type="text" placeholder="TO DATE" name="To" id="To" readonly="readonly" class="form-control" />
                </div>
                <div class="col-md-3">
                    <label>Agency Name</label>
                    <input type="text" class="form-control" id="txtAgencyName" placeholder="Agency Name or ID" name="txtAgencyName" onfocus="focusObj(this);"
                        onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" />
                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                </div>
                <div class="col-md-3 btn-save">
                    <br />
                    <asp:Button ID="btn_search" runat="server" Text="Submit" CssClass="btn cmn-btn" Style="padding: 6px 12px !important; margin: 15px;" />
                    <asp:Button ID="btn_export" runat="server" Text="Export" CssClass="btn cmn-btn" Style="padding: 6px 12px !important; margin: 15px;" />
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-12">
        <div class="input-group">
            <span>Total Record</span>&nbsp; : &nbsp;<asp:Label ID="lblTRecord" runat="server">0</asp:Label>
        </div>
    </div>

    <div class="col-md-12" style="margin-top: -30px;">
        <asp:UpdatePanel ID="up" runat="server">
            <ContentTemplate>
                <asp:GridView ID="Grid_Ledger" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    CssClass="rtable" GridLines="Both" Font-Size="12px" PageSize="30">
                    <Columns>
                        <asp:BoundField DataField="AgencyName" HeaderText="AgencyName" />
                        <asp:BoundField DataField="ReferenceNo" HeaderText="ReferenceNo" />
                        <asp:BoundField DataField="ReservationId" HeaderText="ReservationId" />
                        <asp:BoundField DataField="TotalAmt" HeaderText="Amount" DataFormatString="{0:n}" />
                        <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate" />
                    </Columns>
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>


    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Distributor.js") %>"></script>
</asp:Content>

