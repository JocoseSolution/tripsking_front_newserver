﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterForHome.master" AutoEventWireup="true" CodeFile="UploadPrice.aspx.cs" Inherits="SprReports_Accounts_UploadPrice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script>
        var UrlBase = '<%=ResolveUrl("~/")%>';
    </script>

    <style type="text/css"\>
        .btnsub {
            height: 44px;
            width: 88%;
            color: #68650d;
            background: #ffc000;
            border: none;
        }

        .textboxh {
            height: 44px;
        }

        h3 {
            margin-top: 0px;
            margin-bottom: 5px;
        }

        .btnwidth {
            width: 88%;
        }
    </style>
    <div class="page-title-container">
        <div class="container">
            <div class="page-title pull-left">
                <h2 class="entry-title">Upload Price </h2>
            </div>
        </div>
    </div>

    <div class="container-box" style="padding: 30px !important;">


        <div id="div_start" runat="server" class="row col-md-6" style="margin-left:auto;margin-right:auto;float:none;display:block;background: #575757;padding:35px;color: #fff;box-shadow: 0px 0px 4px #5cb85c; border-radius: 4px;">
            <p class="text-center" style="font-size: 17px; font-weight: bold; color: #ffc000; margin-top: -10px;">Online Money Transfer</p>
            <div class="col-sm-6">
                <asp:Button ID="btn_SendMoney" runat="server" CssClass="btn btn-success btnwidth textboxh"  Text="Send Money" OnClick="btn_SendMoney_Click" />
            </div>
            <div class="col-sm-6">
                <asp:Button ID="btn_Refund" runat="server" Text="Refund" CssClass="btn btn-danger btnwidth textboxh" OnClick="btn_Refund_Click" />
            </div>
        </div>


        <div class="row">
            <div id="div_RefndDetails" runat="server" visible="false"  class="row col-md-6" style="margin-left:auto;margin-right:auto;float:none;display:block;background: #575757;padding:35px;color: #fff;box-shadow: 0px 0px 4px #5cb85c; border-radius: 4px;">
                <p class="text-center" style="font-size: 17px; font-weight: bold; color: #ffc000; margin-top: 14px;"> Check For Refund</p>
                <div class="form-group col-md-9" id="div_trnasid" runat="server">
                    <input type="text" id="TrnsId" runat="server" placeholder="Enter Transcation Id" class="form-control textboxh" onkeypress="return CheckNumericValue(event)" maxlength="12" />
                </div>

                <div class="form-group col-md-3">
                    <%--<asp:Button ID="btn_InitateRefund" runat="server" CssClass="btn btn-default" Text="Intiate Refund" OnClientClick="return CheckTrans_OTP()" OnClick="btn_InitateRefund_Click" />--%>
                    <asp:Button ID="btn_Proceed" runat="server" CssClass="btn btn-success textboxh" Text="Proceed" OnClientClick="return CheckTransID()" OnClick="btn_InitateRefund_Click" />
                </div>
                <asp:Label ID="lbl_iSTransIdValid" runat="server"></asp:Label><br />
                <asp:Label ID="lbl_ResendRefund_otp" runat="server"></asp:Label>
            </div>

            <div runat="server" id="verfiacc" visible="false"  class="row col-md-6" style="margin-left:auto;margin-right:auto;float:none;display:block;background: #575757;padding:35px;color: #fff;box-shadow: 0px 0px 4px #5cb85c; border-radius: 4px;">
             <p class="text-center" style="font-size: 17px; font-weight: bold; color: #ffc000; margin-top: -10px;"> Verify Your Account</p>
            
            <div runat="server" id="div_Otp" class="form-group col-md-6" visible="true">
                <input type="text" id="OTP" runat="server" placeholder="Enter OTP" class="form-control textboxh" onkeypress="return CheckNumericValue(event)" maxlength="10" />
            </div>
                 <div class="form-group col-md-3" runat="server" id="div_Refund" visible="false">
                <%--<asp:Button ID="btn_InitateRefund" runat="server" CssClass="btn btn-default" Text="Intiate Refund" OnClientClick="return CheckTrans_OTP()" OnClick="btn_InitateRefund_Click" />--%>
                <asp:Button ID="btn_Initiate" runat="server" CssClass="btn btn-info textboxh" Text="Initiate Refund" OnClientClick="return CheckTrans_OTP()" OnClick="btn_Initiate_Click" />
            </div>
            <div class="form-group col-md-3" runat="server" id="div_ResendOtp" visible="false">
                <asp:Button ID="ResendRfn_OTP" runat="server" Text="Resend OTP" CssClass="btn btn-danger textboxh" OnClientClick="return CheckTrans_Id()" OnClick="ResendRfn_OTP_Click" />
            </div>

           
                </div>
        </div>

        <div id="div_grid3" runat="server" visible="false">
            <p class="text-center text-info" style="font-size: 20px; border-bottom: 1px solid #ccc; margin-top: 21px;">Check For Refund:</p>
            <asp:Label ID="lbl_RefundStatus" runat="server"></asp:Label>
            <asp:GridView ID="GridView3" runat="server" HeaderStyle-BackColor="SkyBlue" AutoGenerateColumns="false"
                Font-Names="Arial" Font-Size="11pt">
                <Columns>
                    <%--<asp:BoundField DataField="Status" HeaderText="Status" />--%>
                    <asp:BoundField DataField="TID" HeaderText="TID" />
                    <asp:BoundField DataField="Refunded_Amount" HeaderText="Refund Amount" />
                    <asp:BoundField DataField="Commission_Reverse" HeaderText="Commission" />
                    <asp:BoundField DataField="TDS" HeaderText="TDS" />
                </Columns>
            </asp:GridView>
            <asp:Button ID="btn_Reset" runat="server" Text="Reset" OnClick="btn_Reset_Click" CssClass="btn btn-danger" />
        </div>

        <div id="div_grid4" runat="server" visible="false">
            <p class="text-center text-info" style="font-size: 20px; border-bottom: 1px solid #ccc; margin-top: 21px;">Check For Refund:</p>
            <asp:Label ID="lbl_RefundStatus2" runat="server"></asp:Label>
            <asp:GridView ID="GridView4" runat="server" HeaderStyle-BackColor="SkyBlue" AutoGenerateColumns="false"
                Font-Names="Arial" Font-Size="11pt">
                <Columns>
                    <asp:BoundField DataField="TID" HeaderText="TID" />
                    <asp:BoundField DataField="Amount" HeaderText="Amount" />
                    <asp:BoundField DataField="Status" HeaderText="Status" />
                    <asp:BoundField DataField="Type" HeaderText="Type" />
                    <asp:BoundField DataField="TransactionTime" HeaderText="Transaction Time" />
                </Columns>
            </asp:GridView>
            <asp:Button ID="btn_Reset2" runat="server" Text="Reset" OnClick="btn_Reset_Click" CssClass="btn btn-danger" />
        </div>

        <div class="row">
            <div id="div_no" runat="server" visible="false" class="row col-md-6" style="margin-left:auto;margin-right:auto;float:none;display:block;background: #575757;color: #fff;">
                <%-- <asp:Label ID="lbl_ScrhCustomer" runat="server">Search Customer:</asp:Label>--%>
                <p class="text-center" style="font-size: 17px; font-weight: bold; color: #ffc000; margin-top: 14px;">Enter Sender’s Mobile Number</p>
                <div class="form-group col-md-8">
                    <asp:TextBox ID="CustMobile" runat="server" placeholder="Search Customer" CssClass="form-control textboxh" onkeypress="return CheckNumericValue(event)" MaxLength="10"></asp:TextBox>
                    <span>(10 digits) Please don not use prefix zero (0)</span>
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                </div>
                <asp:Label ID="lbl_BankResponse" runat="server"></asp:Label>
                <div id="div_CustName" runat="server" visible="false" class="col-md-3">
                    <div class="form-group">
                        <%--<input type="text" id="CustNames" runat="server" placeholder="Please Enter Name" />--%>
                        <asp:Button ID="btn_RegisterCust" CssClass="btn btn-success" runat="server" Text="Register" OnClientClick="return CheckName()" OnClick="btn_RegisterCust_Click" />
                    </div>
                    <%--<asp:Button ID="btn_OtpVerification" runat="server" Text="Get OTP" OnClick="btn_OtpVerification_Click" />--%>
                </div>

                <div id="div_btn" runat="server" class="col-md-3">
                    <asp:Button ID="SubmitButton" CssClass="btn btn-danger btnsub"  runat="server" Text="Submit" OnClientClick="return CheckNumber()" OnClick="SubmitButton_Click1" />

                </div>
            </div>
        </div>

        <div id="div_EnterOTP" runat="server" visible="false" class="row col-md-6" style="margin-left:auto;margin-right:auto;float:none;display:block;background: #575757;padding:35px;color: #fff;box-shadow: 0px 0px 4px #5cb85c; border-radius: 4px;">
             <p class="text-center" style="font-size: 17px; font-weight: bold; color: #ffc000; margin-top: -10px;"> Verify Your Account</p>
             <div runat="server" id="div2" class="form-group col-md-6" visible="true">
            <input id="txt_OTP" class="form-control textboxh" placeholder="Enter OTP" runat="server" onkeypress="return CheckNumericValue(event)"/>
                 </div>
               <div class="form-group col-md-3">
                  <asp:Button ID="btn_EnterOTP" runat="server" Text="Enter OTP" OnClientClick="return CheckOTP()" OnClick="btn_EnterOTP_Click" CssClass="btn btn-success textboxh" />
                 </div>
            <div class="form-group col-md-3">
            <asp:Button ID="btn_ResendOTP" runat="server" Text="Resend OTP" OnClick="btn_ResendOTP_Click" CssClass="btn btn-info textboxh" />
            <asp:Label ID="lbl_Verify" runat="server"></asp:Label>
             </div>
        </div>


        <div id="div_CustomerInfo" runat="server" visible="false">

            <div class="row" style="text-align: center;">
                <asp:Label runat="server"></asp:Label>
                <h3>Customer Details :</h3>

                <div aria-valuetext="Customer">
                    <asp:Label ID="lbl_CustNAME" runat="server"></asp:Label>
                    <asp:Label ID="lbl_CustMobileNo" runat="server"></asp:Label><br />
                    <br />
                    <br />
                    Available Limit:
            <asp:Label ID="aval_limit" runat="server"></asp:Label>
                    Total Limit:
            <asp:Label ID="total_limit" runat="server"></asp:Label>
                    Message:
            <asp:Label ID="message" runat="server"></asp:Label>
                </div>
            </div>

            <div id="div_BenefList" runat="server" class="table-responsive">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" class="table table-striped table-bordered"
                    Font-Names="Arial" Font-Size="11pt">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:RadioButton ID="RadioButton1" runat="server" OnClick="javascript:check(this.id)" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="name" HeaderText="Name" />
                        <asp:BoundField DataField="Bank" HeaderText="Bank" />
                        <asp:BoundField DataField="Account" HeaderText="Account" />
                        <asp:BoundField DataField="Ifsc" HeaderText="Ifsc" />
                        <asp:BoundField DataField="recipient_id" HeaderText="RecipientId" />
                    </Columns>
                </asp:GridView>

            </div>
            <div class="row">
                <div class="col-md-2">
                    <asp:Button ID="btngetdetails" Text="Start Payment" CssClass="btn btn-default" runat="server" OnClientClick="return CheckBenef()" OnClick="btngetdetails_Click" />
                </div>
                <div id="div_AddDelete" runat="server" visible="false">
                    <asp:Button ID="Button1" runat="server" CssClass="btn btn-default" Text="Add Recipients" OnClick="btn_Add_Click" />
                    <asp:Label ID="lbl_AddRecpResponse" runat="server"></asp:Label>
                    <asp:Button ID="btn_DeleteRecipients" CssClass="btn btn-default" runat="server" Text="Delete Receipents" OnClick="btn_DeleteRecipients_Click" />
                </div>

            </div>


            <div id="div_Payment" runat="server" visible="false">
                <div id="div_ReceDetails" runat="server">
                    <h4>Beneficary Details</h4>
                    <span>Customer Name :</span><asp:Label ID="lbl_BeneName" runat="server"></asp:Label><br />
                    <span>Bank Name :</span><asp:Label ID="lbl_BeneBank" runat="server"></asp:Label><br />
                    <span>Account No. :</span><asp:Label ID="lbl_BeneAccount" runat="server"></asp:Label><br />
                    <span>IFSC Code :</span><asp:Label ID="lbl_BeneIfsc" runat="server"></asp:Label><br />

                </div>
                <div id="div_PaymentChannel" runat="server" visibl="false" class="row">
                    <div class="col-md-2">
                        <input id="txt_Amount" runat="server" class="form-control" placeholder="Amount" onkeypress="return CheckNumericValue(event)" maxlength="5" />
                    </div>
                    <asp:RadioButtonList CssClass="radio-inline" ID="Rdo_Channel" runat="server">
                        <asp:ListItem Text="NEFT" Value="1"></asp:ListItem>
                        <asp:ListItem Text="IMPS" Value="2"></asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:Label ID="lbl_TransResponse" runat="server"></asp:Label><br />
                    <div class="col-md-1">
                        <asp:Button ID="btn_SendMoneyByChannel" runat="server" Text="Proceed" OnClientClick="return CheckAmount()" CssClass="btn btn-default" OnClick="btn_SendMoneyByChannel_Click" />
                    </div>

                    <div id="div_Cancel" runat="server" visible="false">
                        <asp:Button ID="btn_cancel" runat="server" Text="Cancel" CssClass="btn btn-default" OnClick="btn_cancel_Click" />
                    </div>
                </div>
            </div>


            <div id="div_RecepPayInformation" runat="server" visible="false">
                <table>
                    <tr>
                        <td>Beneficary Name:</td>
                        <td class="form-group">
                            <input type="text" id="RecepName" class="form-control" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Bank Account:</td>
                        <td class="form-group">
                            <input type="text" id="RecepAcct" class="form-control" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Bank:</td>
                        <td class="form-group">
                            <input type="text" id="RecepBank" class="form-control" runat="server" /></td>
                    </tr>
                </table>

                <div id="RecepAmount" runat="server">
                    Amount:
                <div class="form-group">
                    <input type="text" id="RecepAmnt" runat="server" class="form-control" onkeypress="return CheckNumericValue(event)" />
                </div>
                </div>
            </div>
        </div>

        <div id="Add_Recepients" visible="false" runat="server" class="row col-md-6" style="margin-left:auto;margin-right:auto;float:none;display:block;background: #575757;padding:35px;color: #fff;box-shadow: 0px 0px 4px #5cb85c; border-radius: 4px;">
            <p class="text-center" style="font-size: 17px; font-weight: bold; color: #ffc000; margin-top: -10px;">Add Recipient</p>
            <br />
            <table class="row">
                <tbody>
                    <tr>
                        <td>Select Bank:</td>
                        <td>
                            <div class="form-group">
                                <asp:DropDownList ID="BankList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="BankName_SelectedIndexChanged" CssClass="form-control" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Account No:</td>
                        <td class="form-group">
                            <input type="text" id="benef_Accountno" runat="server" onkeypress="return CheckNumericValue(event)" class="form-control" maxlength="18" /></td>
                        <asp:Label ID="lbl_beneValidation" runat="server"></asp:Label>
                    </tr>
                    <tr>
                        <td>Beneficary MobileNo:</td>
                        <td class="form-group">
                            <input type="text" id="benef_mobile" runat="server" class="form-control" maxlength="10" onkeypress="return CheckNumericValue(event)" />
                        </td>
                    </tr>
                    <tr>
                        <td>Bank Ifsc:</td>
                        <td class="form-group">
                            <input type="text" id="benef_ifsc" class="form-control" runat="server" maxlength="11" /><asp:Label ID="lbl_isIfscRequired" runat="server"></asp:Label></td>
                        <br />
                    </tr>

                </tbody>
            </table>


            <div id="div_beneNameAndMobile" class="form-group" runat="server" visible="false">
                <table class="row">
                    <tr>
                        <td>Beneficary Name:</td>
                        <td class="form-group">
                            <input type="text" id="benef_name" runat="server" class="form-control" onkeypress="return AllowAlphabet(event)" maxlength="50" />
                        </td>
                    </tr>
                </table>
            </div>

            <div class="row">
                <div id="div1" class="col-md-2" runat="server" visible="false">
                    <asp:Button ID="btn_add" runat="server" Text="Submit" CssClass="btn btn-success" OnClientClick="return CheckForm()" OnClick="btn_add_Click1" />
                </div>

                <div id="div_Validate" class="col-md-2" runat="server">
                    <asp:Button ID="btn_validate" runat="server" CssClass="btn btn-success" Text="Validate" OnClientClick="return CheckForm2()" OnClick="btn_validate_Click" />
                </div>

                <div id="div_Cancel2" class="col-md-2" runat="server">
                    <asp:Button ID="btn_cancel2" runat="server" Text="Cancel" CssClass="btn btn-default" OnClick="btn_cancel_Click" />
                </div>
            </div>
            <asp:Label ID="lbl_RecepientCreated" runat="server" Visible="false"></asp:Label>
        </div>
    </div>
    


    <div id="div_PaymenTResult" runat="server" visible="false">
        <asp:Label ID="lbl_trnsMessage" runat="server"><h3></h3></asp:Label>
        <asp:GridView ID="GridView2" runat="server" CssClass="table table-striped table-bordered" HeaderStyle-BackColor="SkyBlue" AutoGenerateColumns="false"
            Font-Names="Arial" Font-Size="11pt">
            <Columns>
                <asp:BoundField DataField="Status" HeaderText="Status" />
                <asp:BoundField DataField="TID" HeaderText="TID" />
                <asp:BoundField DataField="TDS" HeaderText="TDS" />
                <asp:BoundField DataField="Amount" HeaderText="Amount" />
                <asp:BoundField DataField="Commission" HeaderText="Commission" />
                <asp:BoundField DataField="ModeOfPayment" HeaderText="Mode Of Payment" />
            </Columns>
        </asp:GridView>
        <asp:Button ID="btn_TransStatus" runat="server" Visible="false" CssClass="btn btn-success" Text="Refresh To Get Transaction Status...." OnClick="btn_TransStatus_Click" />
        <asp:Button ID="btn_continueTran" runat="server" Visible="false" CssClass="btn btn-success" Text="Continue Trans" OnClick="btn_continueTran_Click" />
    </div>

    <div class="container" style="margin-top: 50px;">
                <div class="col-sm-12">
                    <div class="row form-group">
                        <div class="col-sm-4">
                            <div class="col-sm-12" style="border-radius: 4px; background: #fff; box-shadow: 0px 0px 4px #000000; min-height: 240px;">
                                <br>
                                <h3 class="text-5 text-center">Step 1</h3>
                                <h3 class="text-5 text-center" style="font-size: 18px !important; border-bottom: 1px solid #000;">Register a Sender</h3>
                                <p>Fill in basic information and register a Sender for Money Transfer. Upgrade Sender to KYC by uploading a Photo Id and an Address Proof.</p>
                                
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="col-sm-12" style="border-radius: 4px; background: #fff; box-shadow: 0px 0px 4px #000000; min-height: 240px;">
                                <br>
                                <h3 class="text-5 text-center">Step 2</h3>
                                <h3 class="text-5 text-center" style="font-size: 18px !important; border-bottom: 1px solid #000;">Add a Beneficiary</h3>
                                <p>Add multiple Beneficiaries / Receivers against each Sender.</p>
                                
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="col-sm-12" style="border-radius: 4px; background: #fff; box-shadow: 0px 0px 4px #000000; min-height: 240px;">
                                <br>
                                <h3 class="text-5 text-center">Step 3</h3>
                                <h3 class="text-5 text-center" style="font-size: 18px !important; border-bottom: 1px solid #000;">Transfer Money</h3>
                                <p>Instantly begin transferring money.</p>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <br />
    <br />
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" language="javascript">
        // SELECT SINGLE RADIO BUTTON ONLY.

        function CheckOTP() {
            if ($('#<%=txt_OTP.ClientID %>').val() == "") {
                alert('Please enter OTP');
                return false;
            }
        }

        function check(objID) {
            var rbSelEmp = $(document.getElementById(objID));
            $(rbSelEmp).attr('checked', true);      // CHECK RADIO BUTTON WHICH IS SELECTED.

            // UNCHECK ALL OTHER RADIO BUTTONS IN THE GRID.
            var rbUnSelect =
                rbSelEmp.closest('table')
                    .find("input[type='radio']")
                    .not(rbSelEmp);

            rbUnSelect.attr('checked', false);
        }

        //$('#btngetdetails').click(function () {

        //    var bSelected = true;
        //    var sEmpName;

        //    // CHECK EACH ROW FOR THE SELECTED RADIO BUTTON.
        //    $('#GridView1')
        //        .find('input:radio')
        //        .each(function () {

        //            var name = $(this).attr('name');

        //            if ($('input:radio[name=' + name + ']:checked').length == 0) {
        //                bSelected = false
        //            }
        //            else {
        //                sEmpName = $('input:radio[name=' + name + ']:checked').closest('tr')
        //                    .children('td:nth-child(3)')
        //                    .map(function () {

        //                        return $.trim($(this).text());
        //                    }).get();
        //            }
        //        });

        //    // FINALLY SHOW THE MESSAGE.
        //    if (bSelected == false) {
        //        alert('Invalid Selection'); return false
        //    }
        //    else { }
        //});


        function CheckBenef() {
            var bSelected = true;
            var sEmpName;

            // CHECK EACH ROW FOR THE SELECTED RADIO BUTTON.
            $('#GridView1')
                .find('input:radio')
                .each(function () {

                    var name = $(this).attr('name');

                    if ($('input:radio[name=' + name + ']:checked').length == 0) {
                        bSelected = false
                    }
                    else {
                        sEmpName = $('input:radio[name=' + name + ']:checked').closest('tr')
                            .children('td:nth-child(3)')
                            .map(function () {

                                return $.trim($(this).text());
                            }).get();
                    }
                });

            // FINALLY SHOW THE MESSAGE.
            if (bSelected == false) {
                alert('Invalid Selection'); return false
            }
            else { }
        }

        function CheckNumericValue(e) {
            debugger;
            var key = e.which ? e.which : e.keyCode;
            //enter key  //backspace //tabkey      //escape key                  
            if ((key >= 48 && key <= 57) || key == 13 || key == 8 || key == 9 || key == 27) {
                return true;
            }
            else {
                alert("Please Enter Number Only");
                return false;
            }
        }

        function CheckTrans_Id() {
            if ($('#<%=TrnsId.ClientID %>').val() == "") {
                alert('Please Fill Transcation Id');
                return false;
            }
            return true;
        }

        function CheckTrans_OTP() {
            if ($('#<%=TrnsId.ClientID %>').val() == "") {
                alert('Please Fill Transcation Id');
                return false;
            }
            if ($('#<%=OTP.ClientID %>').val() == "") {
                alert('Please Fill OTP');
                return false;
            }
            return true;
        }


        function CheckAmount() {
            debugger;

            var checked_radio = $("[id*=Rdo_Channel] input:checked");
            if ($('#<%=txt_Amount.ClientID %>').val() == "") {
                alert('Please Fill Appropriate amount.');
                return false;
            }


            var rb = document.getElementById("<%=Rdo_Channel.ClientID%>");
            var radio = rb.getElementsByTagName("input");
            var isChecked = false;
            for (var i = 0; i < radio.length; i++) {
                if (radio[i].checked) {
                    isChecked = true;
                    break;
                }
            }
            if (!isChecked) {
                alert("Please select payment option.");
                return false;
            }

            //return isChecked;


            //if ($('#Rdo_Channel :radio:checked').length <= 0) {
            //    alert('Please select channel!');
            //    return false;
            //}
        }

        function CheckTransID() {
            if ($('#<%=TrnsId.ClientID %>').val() == "") {
                alert('Please Fill Transcation Id');
                return false;
            }
            return true;
        }

        function CheckForm2() {
            if ($('#<%=BankList.ClientID %>').val() == "0") {
                alert('Please Select Bank');
                return false;
            }

            if ($('#<%=benef_Accountno.ClientID %>').val() == "") {
                alert('Please Fill Account No');
                return false;
            }
            if ($('#<%=benef_mobile.ClientID %>').val() == "") {
                alert('Please Fill Mobile No');
                return false;
            }
            return true;
        }



        function CheckNumber() {
            if ($('#<%=CustMobile.ClientID %>').val() == "") {
                alert('Please enter valid number.');
                return false;
            }
        }

        function AllowAlphabet(e) {
            isIE = document.all ? 1 : 0
            keyEntry = !isIE ? e.which : event.keyCode;
            if (((keyEntry >= '65') && (keyEntry <= '90')) || ((keyEntry >= '97') && (keyEntry <= '122')) || (keyEntry == '46') || (keyEntry == '32') || keyEntry == '45')
                return true;
            else {
                alert('Please Enter Only Character values.');
                return false;
            }
        }


        function CheckForm() {
            if ($('#<%=BankList.ClientID %>').val() == "0") {
                alert('Please Select Bank');
                return false;
            }

            if ($('#<%=benef_Accountno.ClientID %>').val() == "") {
                alert('Please Fill Account No');
                return false;
            }
            if ($('#<%=benef_mobile.ClientID %>').val() == "") {
                alert('Please Fill Mobile No');
                return false;
            }
            if ($('#<%=benef_ifsc.ClientID %>').val() == "") {
                alert('Please Fill IFSC');
                return false;
            }
            if ($('#<%=benef_name.ClientID %>').val() == "") {
                alert('Please Fill Name');
                return false;
            }
            return true;
        }

    </script>

</asp:Content>







