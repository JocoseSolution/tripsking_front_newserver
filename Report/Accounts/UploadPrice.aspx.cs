﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//using TripsKingPaymentGate;
using System.Net;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;
using System.Data;
using DMT;
//using System.Device.Location;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;



public partial class SprReports_Accounts_UploadPrice : System.Web.UI.Page
{

    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    SqlTransaction objDA = new SqlTransaction();
    DataSet AgencyDs = new DataSet();
    Dictionary<string, string> Log = new Dictionary<string, string>();
    Dictionary<string, string> DeletedLog = new Dictionary<string, string>();
    private static Int64 TransactionID;
    Eko Keys = new Eko();
    JavaScriptSerializer ser = new JavaScriptSerializer();
    CreateCustomer NewCustomerResponse = new CreateCustomer();
    GetCustmerInfo CustomerResponse = new GetCustmerInfo();
    GetAllRecepients receipents = new DMT.GetAllRecepients();
    RecepientStatus RecepStatus = new RecepientStatus();
    BankResponse BankResponse = new BankResponse();
    BeeficaryCollection isBenficaryVerified = new BeeficaryCollection();
    CustomerOTPCollection CustomerOTPVer = new CustomerOTPCollection();
    CutomerVerification CustVerification = new CutomerVerification();
    Bank Banks = new Bank();
    PayGateReq Fetch = new PayGateReq();
    TransResponse trnsResponse = new TransResponse();
    DataAccessLayer dtLayer = new DataAccessLayer();
    CustomerOTPCollection OTPResponse = new CustomerOTPCollection();
    RecipientDeleted isReipientDelted = new RecipientDeleted();
    SplitResponse SplitResponse = new SplitResponse();
    DataAccessLayer AccessLayer = new DataAccessLayer();
    RefundEvalue RefundEvalue = new RefundEvalue();
    ResendRefundOTP GetRefundOTP = new ResendRefundOTP();
    TransStatus ChkIfTransStatusIsRefund = new TransStatus();
    PayGateReq.RequestUserdetails Details = new PayGateReq.RequestUserdetails();
    TransAwaited TransAwaitedStatus = new TransAwaited();
    static List<TransSummary> Transsummary = new List<TransSummary>();
    public static bool Check = true;
    DataTable dtgetdetail = new DataTable();


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == "" || Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        //if (!IsPostBack == true)
        //{

        //}
        Label1.Text = "";
    }

    private static string GenererteUniqueNo()
    {
        string Random = "";
        for (int Rd = 0; Rd < 9; Rd++)
        {
            Random rd = new Random();
            Random += rd.Next(0, 9).ToString();
        }
        return Random;
    }

    protected void btn_SendMoney_Click(object sender, EventArgs e)
    {
        div_start.Visible = false;
        div_no.Visible = true;

    }

    protected void SubmitButton_Click1(object sender, EventArgs e)
    {
		TransactionID = Convert.ToInt64(GenererteUniqueNo());
        try
        {
            
            PayGateReq.RequestUserdetails.CustomerMobile = CustMobile.Text;
            SequrityKeys ks = Eko.GetSequrityKeys;
            PayGateReq.RequestUserdetails.Secrect_Key = ks.secret_key;
            PayGateReq.RequestUserdetails.secretKeyTimestamp = ks.secret_key_timestamp;
            ViewState["mobilenumber"] = CustMobile.Text;
            dynamic limit = null;
            string CustomerResp = "";
            dtgetdetail = getuserid(Session["UID"].ToString());

            if (dtgetdetail.Rows.Count != 0)
            {
                Session["UserCode"] = dtgetdetail.Rows[0]["User_Code"].ToString();


                if (!string.IsNullOrEmpty(Session["UserCode"].ToString()))
                {
                    //PayGateReq.RequestUserdetails.UserCode = ConfigurationManager.AppSettings["UserCode"];
                    string Url = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile + "?initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&user_code=" + Session["UserCode"].ToString() + "";
                    CustomerResp = Fetch.GetCustomer(Url, "", "GET", "GetCustomerInfo", TransactionID);
                    CustomerResponse = ser.Deserialize<GetCustmerInfo>(CustomerResp);
                    if (!string.IsNullOrEmpty(CustomerResp))
                    {
                        if (CustomerResp.Contains("Customer does not exist in System"))
                        {
                            lbl_BankResponse.Visible = false;
                            div_btn.Visible = false;
                            div_CustName.Visible = true;

                        }
                        else if (!CustomerResponse.message.Contains("Verification pending"))
                        {
                            limit = CustomerResponse.data.limit.Where(x => x.pipe == "9").ToList();
                        }
                    }
                }

                //if (CustomerResp.Contains("bc_available_limit"))
                //{
                //    CustomerResponse = ser.Deserialize<GetCustmerInfo>(CustomerResp);

                //}
                //Log.Add("Customerinfo_Req", Url);
                //Log.Add("Customerinfo_Resp", CustomerResp);
                //LogWriter.RequestResLog(Url, CustomerResp, "CustomerLogin",TransactionID);
                if (CustomerResp.Contains("User Code cannot be null") || string.IsNullOrEmpty(Session["UserCode"].ToString()))
                {
                    Label1.Text = "User id does not exist Onboard First. Onboard Link <a href='https://tripsking.in/Report/Accounts/Agentonboard.aspx'>Agent Onboard</a>";
                }
                //if (CustomerResponse.message == null || CustomerResponse.message == "")
                //{
                //    lbl_BankResponse.Text = "Bank server not responding";
                //}
                else if (CustomerResponse.message.Contains("Verification pending"))
                {
                    string urlotp = serviceUrl.otpurl;
                    //string BODY = "initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&mobile=" + PayGateReq.RequestUserdetails.CustomerMobile + "&user_code=" + UserCode + "";
                    //string CustomerOTPVerresp = Fetch.GetCustomer(urlotp, BODY, "PUT", "VerifyCustomerUsingOTP", TransactionID);
                    //CustomerOTPVer = ser.Deserialize<CustomerOTPCollection>(CustomerOTPVerresp);
                    string Urlforotp = "" + serviceUrl.getcustomer + "mobile_number:" + PayGateReq.RequestUserdetails.CustomerMobile + "";
                    string BODY = "initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&user_code=" + Session["UserCode"].ToString() + "&pipe=9&name=" + dtgetdetail.Rows[0]["FirstName"].ToString() + "&residence_address={\"line\":\"EkoIndia\",\"city\":\"" + dtgetdetail.Rows[0]["City"].ToString() + "\",\"state\":\"" + dtgetdetail.Rows[0]["State"].ToString() + "\",\"pincode\":\"" + dtgetdetail.Rows[0]["Pincode"].ToString() + "\",\"district\":\"" + dtgetdetail.Rows[0]["District"].ToString() + "\",\"area\":\"" + dtgetdetail.Rows[0]["Area"].ToString() + "\"}&dob=" + dtgetdetail.Rows[0]["DOB"].ToString() + "";
                    string OTPResp = Fetch.GetCustomer(Urlforotp, BODY, "PUT", "ResendOTPToRegisterNewCust", TransactionID);
                    NewCustomerResponse = ser.Deserialize<CreateCustomer>(OTPResp);
                    // Log.Add("CustOTPVerOnLogin_Req", urlotp);
                    // Log.Add("CustOTPVerOnLogin_Resp", CustomerOTPVerresp);
                    int kd = insertotpforcustomer(Session["UserCode"].ToString(), NewCustomerResponse.data.customer_id, NewCustomerResponse.data.otp_ref_id, NewCustomerResponse.response_type_id, NewCustomerResponse.message);
                    SubmitButton.Visible = false;
                    div_no.Visible = false;
                    txt_OTP.Value = NewCustomerResponse.data.otp;
                    div_EnterOTP.Visible = true;
                    lbl_Verify.Text = NewCustomerResponse.message;
                }


                else if (limit[0].is_registered == 0)
                {
                    lbl_BankResponse.Visible = false;
                    div_btn.Visible = false;
                    div_CustName.Visible = true;

                }

                else if (CustomerResponse.message.Contains("Non-KYC active") || CustomerResponse.message.Contains("Sender already registered"))
                {
                    lbl_BankResponse.Visible = false;
                    string urlgetrecip = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile + "/recipients?initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&user_code=" + Session["UserCode"].ToString() + "";
                    string receipentsResp = Fetch.GetCustomer(urlgetrecip, "", "GET", "GetCustomerBenefeciaries", TransactionID);
                    receipents = ser.Deserialize<GetAllRecepients>(receipentsResp);
                    // Log.Add("GetCustBenef_Req", urlgetrecip);
                    // Log.Add("GetCustBenef_Resp", receipentsResp);
                    if (receipents.message.Contains("No recepients found"))
                    {
                        div_CustomerInfo.Visible = true;
                        lbl_CustNAME.Text = Convert.ToString(CustomerResponse.data.name);
                        lbl_CustMobileNo.Text = Convert.ToString(CustomerResponse.data.mobile);
                        total_limit.Text = Convert.ToString(CustomerResponse.data.total_limit);
                        aval_limit.Text = Convert.ToString(CustomerResponse.data.available_limit);
                        btngetdetails.Visible = false;//by default false
                        div_no.Visible = false;
                        div_AddDelete.Visible = true;
                        Button1.Visible = true;
                        lbl_AddRecpResponse.Visible = false;
                        btn_DeleteRecipients.Visible = false;

                    }
                    else if (receipents.data != null)
                    { bindvalue(); }
                }
            }
            else
            {
                Label1.Text = "User id does not exist Onboard First. Onboard Link <a href='https://tripsking.in/Report/Accounts/Agentonboard.aspx'>Agent Onboard</a>";
            }
            //else if (CustomerResponse.message.Contains("customer_id does not exist in system"))
            //else if (CustomerResponse.data.limit[2].is_registered==0)
            //{
            //    lbl_BankResponse.Visible = false;
            //    div_btn.Visible = false;
            //    div_CustName.Visible = true;

            //}
        }
        catch (Exception ex)
        {
            LogWriter.ErrorLog(ex.Message.ToString(), "SubmitButton_Click1", Convert.ToInt64(TransactionID));
        }
    }


    public void bindvalue()
    {
        try
        {
            List<RecipientEntity> recepientList = new List<RecipientEntity>();
            div_CustomerInfo.Visible = true;
            lbl_CustNAME.Text = Convert.ToString(CustomerResponse.data.name);
            lbl_CustMobileNo.Text = Convert.ToString(CustomerResponse.data.mobile);
            total_limit.Text = Convert.ToString(CustomerResponse.data.total_limit);
            aval_limit.Text = Convert.ToString(CustomerResponse.data.available_limit);
            message.Text = CustomerResponse.message;
            div_no.Visible = false;
            div_btn.Visible = false;

            if (receipents.data.recipient_list != null)
            {
                if (receipents.data.recipient_list.Count > 0)
                {

                    recepientList = new List<RecipientEntity>();
                    for (int i = 0; i < receipents.data.recipient_list.Count; i++)
                    {
                        recepientList.Add(new RecipientEntity { name = receipents.data.recipient_list[i].recipient_name, Account = receipents.data.recipient_list[i].account, Ifsc = receipents.data.recipient_list[i].ifsc, Bank = receipents.data.recipient_list[i].bank, recipient_id = receipents.data.recipient_list[i].recipient_id });
                    }
                    //div_BenefList.Visible = true;
                    GridView1.Visible = true;
                    GridView1.DataSource = recepientList.ToList();
                    GridView1.DataBind();
                    div_AddDelete.Visible = true;
                    //Button1.Visible = true;
                    //btn_DeleteRecipients.Visible = true;
                    //btngetdetails.Visible = true;
                }
            }
            else
            {
                div_AddDelete.Visible = true;
                Button1.Visible = true;
                lbl_AddRecpResponse.Visible = false;
                btn_DeleteRecipients.Visible = false;
                btngetdetails.Visible = false;
            }
        }
        catch (Exception ex)
        { LogWriter.ErrorLog(ex.Message.ToString(), "BindValue", Convert.ToInt64(TransactionID)); }

    }

    protected void btn_Add_Click(object sender, EventArgs e)
    {
        try
        {

            div_CustomerInfo.Visible = false;
            div_Validate.Visible = false;
            Add_Recepients.Visible = true;
            div1.Visible = false;
            lbl_RecepientCreated.Visible = false;
            lbl_isIfscRequired.Visible = false;
            benef_Accountno.Value = "";
            benef_mobile.Value = "";
            benef_ifsc.Value = "";
            DataSet DS = AccessLayer.BindBankDropdown();
            BankList.DataSource = DS;
            BankList.DataTextField = " BANK_NAME ";
            BankList.DataValueField = " bank_code ";
            BankList.DataBind();
            BankList.Items.Insert(0, new ListItem("Please Select Bank", "0"));

        }
        catch (Exception ex)
        { LogWriter.ErrorLog(ex.Message.ToString(), "btn_Add_Click", Convert.ToInt64(TransactionID)); }
    }


    protected void btn_add_Click1(object sender, EventArgs e)
    {
        try
        {

            div_Cancel.Visible = false;
            Add_Recepients.Visible = false;
            div1.Visible = false;
            div_beneNameAndMobile.Visible = false;
            string AddRecipientURL = string.Empty;
            string AccountNo = benef_Accountno.Value;  //Request.Form["benef_Accountno"];
            PayGateReq.RequestUserdetails.Ifsc = benef_ifsc.Value; // Request.Form["benef_ifsc"];
            string BenefeciaryName = benef_name.Value; //Request.Form["benef_name"];
            string BenefeciaryMobile = benef_mobile.Value; //Request.Form["benef_mobile"];
            string recipient_id_type_Ifsc = AccountNo + "_" + PayGateReq.RequestUserdetails.Ifsc;
            string recipient_id_type = AccountNo + "_" + PayGateReq.RequestUserdetails.bankCode;
            string BankName = BankList.SelectedItem.Text;
            PayGateReq.RequestUserdetails.bankCode = BankList.SelectedValue;
            string BankCode = PayGateReq.RequestUserdetails.bankCode;
            int BankId = Convert.ToInt16(dtLayer.GetBankId(BankCode));

            dtLayer.NewlyAddedBenef(Convert.ToInt64(PayGateReq.RequestUserdetails.Initatior_Id), "", BenefeciaryName, Convert.ToInt64(BenefeciaryMobile), PayGateReq.RequestUserdetails.Ifsc, PayGateReq.RequestUserdetails.AccountNo, BankName, Convert.ToInt32(BankId));
            if (recipient_id_type_Ifsc != null)
            {

                AddRecipientURL = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile + "/recipients/acc_ifsc:" + recipient_id_type_Ifsc + "";
            }
            else
            { AddRecipientURL = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile + "/recipients/acc_bankcode:" + recipient_id_type + ""; }
            string Url = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile + "?initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&user_code=" + Session["UserCode"].ToString() + "";
            string CustomerResp = Fetch.GetCustomer(Url, "", "GET", "CheckIfCustIsRegisterd", TransactionID);
            CustomerResponse = ser.Deserialize<GetCustmerInfo>(CustomerResp);
            //Log.Add("GetCustInfoONAdd_Req", Url);
            //Log.Add("GetCustInfoONAdd_Resp", CustomerResp);
            if (CustomerResponse.status == 0)
            {

                string body = "initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&recipient_mobile=" + BenefeciaryMobile + "&bank_id=" + BankId + "&recipient_type=3&recipient_name=" + BenefeciaryName + "&user_code=" + Session["UserCode"].ToString() + "";
                string RecepStatusResponse = Fetch.GetCustomer(AddRecipientURL, body, "PUT", "AddBenefciary", TransactionID);
                RecepStatus = ser.Deserialize<RecepientStatus>(RecepStatusResponse);
                //Log.Add("IfRecipientAdd_Req", AddRecipientURL);
                //Log.Add("IfRecipientAdd_Resp", RecepStatusResponse);
                if (RecepStatus.status == 0)
                {
                    string urlgetrecip = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile + "/recipients?initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&user_code=" + Session["UserCode"].ToString() + "";
                    string receipentsResp = Fetch.GetCustomer(urlgetrecip, "", "GET", "", TransactionID);
                    receipents = ser.Deserialize<GetAllRecepients>(receipentsResp);
                    bindvalue();
                    div_AddDelete.Visible = true;
                }
                if (RecepStatus.status == 45 || RecepStatus.status == 44)
                {
                    Add_Recepients.Visible = true;
                    lbl_RecepientCreated.Visible = true;
                    div_beneNameAndMobile.Visible = true;
                    div1.Visible = true;
                    lbl_RecepientCreated.Text = RecepStatus.message;
                }
                if (RecepStatus.status == 46)
                {
                    Add_Recepients.Visible = true;
                    lbl_RecepientCreated.Visible = true;
                    div_beneNameAndMobile.Visible = true;
                    lbl_RecepientCreated.Text = RecepStatus.message;
                    div_Cancel.Visible = true;
                }
            }
            else if (CustomerResponse.status == 463)
            {
                //string CreateCustURL = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + BenefeciaryMobile;
                //string Body = "initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&name=" + BenefeciaryName + "&user_code=" + UserCode + "";

                string CreateCustURL = "" + serviceUrl.getcustomer + "mobile_number:" + BenefeciaryMobile + "";
                string Body = "initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&user_code=" + Session["UserCode"].ToString() + "&pipe=9&name=" + dtgetdetail.Rows[0]["FirstName"].ToString() + "&residence_address={\"line\":\"EkoIndia\",\"city\":\"" + dtgetdetail.Rows[0]["City"].ToString() + "\",\"state\":\"" + dtgetdetail.Rows[0]["State"].ToString() + "\",\"pincode\":\"" + dtgetdetail.Rows[0]["Pincode"].ToString() + "\",\"district\":\"" + dtgetdetail.Rows[0]["District"].ToString() + "\",\"area\":\"" + dtgetdetail.Rows[0]["Area"].ToString() + "\"}&dob=" + dtgetdetail.Rows[0]["DOB"].ToString() + "";
                string NewCustResponse = Fetch.GetCustomer(CreateCustURL, Body, "PUT", "RegisterNewCustomer", TransactionID);
                NewCustomerResponse = ser.Deserialize<CreateCustomer>(NewCustResponse);
                if (NewCustomerResponse.status != 0)
                {
                    Add_Recepients.Visible = true;
                    div1.Visible = true;
                    btn_add.Visible = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('" + NewCustomerResponse.message + "');", true);
                }
                //Log.Add("CreateNewCust_Req", CreateCustURL);
                //Log.Add("CreateNewCust_Resp", NewCustResponse);
                if (NewCustomerResponse.status == 0)
                {
                    string body = "initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&recipient_mobile=" + BenefeciaryMobile + "&bank_id=" + BankId + "&recipient_type=3&recipient_name=" + BenefeciaryName + "&user_code=" + Session["UserCode"].ToString() + "";
                    string RecepStatusResponse = Fetch.GetCustomer(AddRecipientURL, body, "PUT", "", TransactionID);
                    RecepStatus = ser.Deserialize<RecepientStatus>(RecepStatusResponse);
                    if (RecepStatus.status == 0)
                    {
                        string urlgetrecip = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile + "/recipients?initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&user_code=" + Session["UserCode"].ToString() + "";
                        string receipentsResp = Fetch.GetCustomer(urlgetrecip, "", "GET", "GetCustomerBenefeciaries", TransactionID);
                        receipents = ser.Deserialize<GetAllRecepients>(receipentsResp);
                        bindvalue();
                    }
                    else if (RecepStatus.status == 46)
                    {
                        Add_Recepients.Visible = true;
                        lbl_RecepientCreated.Visible = true;
                        div_beneNameAndMobile.Visible = true;
                        lbl_RecepientCreated.Text = RecepStatus.message;
                    }
                    else
                    {
                        Add_Recepients.Visible = true;
                        div_beneNameAndMobile.Visible = true;
                        lbl_RecepientCreated.Visible = true;
                        lbl_RecepientCreated.Text = RecepStatus.message;
                    }
                }
                //else if (NewCustomerResponse.status == 17)
                //{
                //    lbl_RecepientCreated.Text = "Sender Already Registerd";
                //}
            }
            else
            { lbl_RecepientCreated.Text = RecepStatus.message; }
        }
        catch (Exception ex)
        { LogWriter.ErrorLog(ex.Message.ToString(), "btn_add_Click1", Convert.ToInt64(TransactionID)); }
    }



    protected void btn_validate_Click(object sender, EventArgs e)
    {
        try
        {
            PayGateReq.RequestUserdetails.AccountNo = benef_Accountno.Value; //Request.Form["benef_Accountno"];
            PayGateReq.RequestUserdetails.Ifsc = benef_ifsc.Value;
            PayGateReq.RequestUserdetails.RecpMobileNo = Convert.ToString(benef_mobile.Value);
            PayGateReq.RequestUserdetails.bankCode = BankList.SelectedValue;
            PayGateReq.RequestUserdetails.bankCode = PayGateReq.RequestUserdetails.bankCode.Replace(" ", String.Empty);
            if (!string.IsNullOrEmpty(PayGateReq.RequestUserdetails.AccountNo) && !string.IsNullOrEmpty(PayGateReq.RequestUserdetails.bankCode))
            {

                string ValidateNewBeneficaryURL = "" + serviceUrl.ValidateCustomer + "" + PayGateReq.RequestUserdetails.bankCode + "/accounts/" + PayGateReq.RequestUserdetails.AccountNo + "";
                string body = "customer_id=" + PayGateReq.RequestUserdetails.RecpMobileNo + "&initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&user_code=" + Session["UserCode"].ToString();
                string NewBenefResp = Fetch.GetCustomer(ValidateNewBeneficaryURL, body, "POST", "ValidateNewBenefciary", TransactionID);
                isBenficaryVerified = ser.Deserialize<BeeficaryCollection>(NewBenefResp);
                //  Log.Add("ValidateNewBenef_Req", ValidateNewBeneficaryURL);
                //Log.Add("ValidateNewBenef_Resp", NewBenefResp);
                if (isBenficaryVerified.message.Contains("Success!  Account details found") || isBenficaryVerified.status == 0)
                {
                    PayGateReq.RequestUserdetails.ClientRefId = isBenficaryVerified.data.client_ref_id;
                    div_Validate.Visible = false;
                    div_beneNameAndMobile.Visible = true;
                    div1.Visible = true;
                    benef_name.Value = isBenficaryVerified.data.recipient_name;
                }
                else
                {
                    lbl_isIfscRequired.Visible = false;
                    div_Validate.Visible = false;
                    div_beneNameAndMobile.Visible = true;
                    div1.Visible = true;
                    benef_name.Visible = true;
                }
            }
        }
        catch (Exception ex)
        { LogWriter.ErrorLog(ex.Message.ToString(), "btn_validate_Click", Convert.ToInt64(TransactionID)); }
    }


    protected void btn_EnterOTP_Click(object sender, EventArgs e)
    {
        try
        {
            string OTP = txt_OTP.Value;
            string MobileNo = CustMobile.Text;
            int ijk = updateotp(Session["UserCode"].ToString(), MobileNo, OTP, "");
            string ValidateOTPURL = "" + serviceUrl.VerifyOTP + "" + OTP + "";
            DataTable dtrefotp = getotpref(MobileNo);
            //string BODY = "initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&id_type=" + PayGateReq.RequestUserdetails.customer_id_type + "&id=" + PayGateReq.RequestUserdetails.CustomerMobile + "&user_code=" + UserCode + "";
            string BODY = "initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&id_type=mobile_number&id=" + MobileNo + "&user_code=" + Session["UserCode"].ToString() + "&pipe=9&otp_ref_id=" + dtrefotp.Rows[0]["otp_ref_id"] + "";
            string VerifyCustOTP = Fetch.GetCustomer(ValidateOTPURL, BODY, "PUT", "VerifyCustOTP", TransactionID);
            CustVerification = ser.Deserialize<CutomerVerification>(VerifyCustOTP);

            // Log.Add("ValidateCustByOTP_Req", ValidateOTPURL);
            // Log.Add("ValidateCustByOTP_Resp", VerifyCustOTP);
            if (CustVerification.status == 303)
            {
                lbl_Verify.Text = CustVerification.message;
                div_EnterOTP.Visible = true;
            }
            else if (CustVerification.status == 302)
            {
                lbl_Verify.Text = CustVerification.message;
            }
            else if (CustVerification.status == 0 && (CustVerification.message.Contains("Wallet opened successfully") || CustVerification.message.Contains("Customer already verified") || CustVerification.message.Contains("Customer registered on the bank")))
            {
                div_EnterOTP.Visible = false;

                string Url = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile + "?initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&user_code=" + Session["UserCode"].ToString() + "";
                string CustomerResp = Fetch.GetCustomer(Url, "", "GET", "", TransactionID);
                CustomerResponse = ser.Deserialize<GetCustmerInfo>(CustomerResp);

                if (CustomerResponse.status == 0 || CustomerResponse.message.Contains("Non-KYC active") || CustomerResponse.message.Contains("Sender already registered"))
                {
                    lbl_BankResponse.Visible = false;
                    btngetdetails.Visible = true;
                    btn_DeleteRecipients.Visible = false;
                    string urlgetrecip = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile + "/recipients?initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&user_code=" + Session["UserCode"].ToString() + "";
                    string receipentsResp = Fetch.GetCustomer(urlgetrecip, "", "GET", "GetCustomerBenefeciaries", TransactionID);
                    receipents = ser.Deserialize<GetAllRecepients>(receipentsResp);
                    bindvalue();
                }
            }
        }
        catch (Exception ex)
        { LogWriter.ErrorLog(ex.Message.ToString(), "btn_EnterOTP_Click", Convert.ToInt64(TransactionID)); }
    }


    protected void btn_ResendOTP_Click(object sender, EventArgs e)
    {
        try
        {
            //string Url = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile + "/otp";
            //string BODY = "initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id;

            string Url = "" + serviceUrl.getcustomer + "mobile_number:" + PayGateReq.RequestUserdetails.CustomerMobile + "";
            string BODY = "initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&user_code=" + Session["UserCode"].ToString() + "&pipe=9&name=" + dtgetdetail.Rows[0]["FirstName"].ToString() + "&residence_address={\"line\":\"EkoIndia\",\"city\":\"" + dtgetdetail.Rows[0]["City"].ToString() + "\",\"state\":\"" + dtgetdetail.Rows[0]["State"].ToString() + "\",\"pincode\":\"" + dtgetdetail.Rows[0]["Pincode"].ToString() + "\",\"district\":\"" + dtgetdetail.Rows[0]["District"].ToString() + "\",\"area\":\"" + dtgetdetail.Rows[0]["Area"].ToString() + "\"}&dob=" + dtgetdetail.Rows[0]["DOB"].ToString() + "";
            string OTPResp = Fetch.GetCustomer(Url, BODY, "POST", "ResendOTPToRegisterNewCust", TransactionID);
            OTPResponse = ser.Deserialize<CustomerOTPCollection>(OTPResp);
            int ijk = updateotp(Session["UserCode"].ToString(), ViewState["mobilenumber"].ToString(), OTPResponse.data.otp, OTPResponse.data.otp_ref_id);
            // Log.Add("ResendOTP_Req", Url);
            //  Log.Add("ResendOTP_Resp", OTPResp);
            if (OTPResponse.status == 0)  //(OTPResponse.message.Contains("success! OTP processed") || OTPResponse.message.Contains("OTP Resent"))
                txt_OTP.Value = OTPResponse.data.otp;
            else if (OTPResponse.message.Contains("Sender already registered"))
            {
                string CustUrl = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile + "?initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&user_code=" + Session["UserCode"].ToString() + "";
                string CustomerResp = Fetch.GetCustomer(CustUrl, "", "GET", "", TransactionID);
                CustomerResponse = ser.Deserialize<GetCustmerInfo>(CustomerResp);
                if (CustomerResponse.message == null || CustomerResponse.message == "")
                {
                    lbl_BankResponse.Text = "Bank server not responding";
                }

                else if (CustomerResponse.message.Contains("Non-KYC active") || CustomerResponse.message.Contains("Sender already registered"))
                {
                    lbl_BankResponse.Visible = false;
                    string urlgetrecip = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile + "/recipients?initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&user_code=" + Session["UserCode"].ToString() + "";
                    string receipentsResp = Fetch.GetCustomer(urlgetrecip, "", "GET", "GetCustomerBenefeciaries", TransactionID);
                    receipents = ser.Deserialize<GetAllRecepients>(receipentsResp);
                    bindvalue();
                }
                else if (OTPResponse.status == 463 && (CustomerResponse.message.Contains("customer_id does not exist in system")))
                {
                    lbl_BankResponse.Visible = false;
                    div_btn.Visible = false;

                }
            }
        }
        catch (Exception ex)
        { LogWriter.ErrorLog(ex.Message.ToString(), "btn_ResendOTP_Click", Convert.ToInt64(TransactionID)); }
    }

    protected void btn_SendMoneyByChannel_Click(object sender, EventArgs e)
    {
        try
        {
            div_Cancel.Visible = false;
            Int64 splitId = 0;
            string Body = string.Empty;
            int Count = 0;
            bool flag = true;
            bool chktrns = true;
            string Channel = Rdo_Channel.SelectedItem.Value.ToString();
            double Amount = Convert.ToDouble(txt_Amount.Value);
            string Currency = "INR";
            string State = "1";
            String timeStamp = GetTimestamp(DateTime.Now);
            string LatLong = GetLatLon();
            Transsummary.Clear();
            txt_Amount.Value = "";
            Rdo_Channel.ClearSelection();

            string uniqueclientrefid = uniquekey();
            AgencyDs = objDA.GetAgencyDetails(Session["UID"].ToString());
            if ( Amount>=500)
            {
                if (Convert.ToDouble(AgencyDs.Tables[0].Rows[0]["Crd_Limit"].ToString().Trim()) > Amount)
                {

                    //List<TransSummary> Transsummary = new List<TransSummary>();
                    if (Amount > 5000)
                    {
                        string SplitIdURL = "" + serviceUrl.SplitId + "" + PayGateReq.RequestUserdetails.Initatior_Id + "&customer_id=" + PayGateReq.RequestUserdetails.CustomerMobile + "&recipient_id=" + PayGateReq.RequestUserdetails.recipient_id + "&amount=" + Amount + "&channel=" + Channel + "&user_code=" + Session["UserCode"].ToString() + "";

                        string SplitResp = Fetch.GetCustomer(SplitIdURL, "", "GET", "GetSplitid", TransactionID);
                        SplitResponse = ser.Deserialize<SplitResponse>(SplitResp);
                        // Log.Add("GetSplitId_Req", SplitIdURL);
                        //  Log.Add("GetSplitId_Resp", SplitResp);
                        if (SplitResponse.status == 0)
                        {
                            splitId = SplitResponse.data.split_tid;
                        }
                        else
                        {
                            flag = false;
                            Rdo_Channel.ClearSelection();
                            lbl_TransResponse.Text = SplitResponse.message;
                        }
                    }

                    if (flag)
                    {
                        if (Amount > 5000)
                        {
                            if (SplitResponse.data.splitTransactionList.Count > 0)
                            {
                                foreach (var Amt in SplitResponse.data.splitTransactionList)
                                {
                                    if (chktrns)
                                    {
                                        Amount = Convert.ToDouble(Amt.split_amount);
                                        string SendMoneyURL = "" + serviceUrl.SendMoney + "";
                                        if (Channel.Contains("1"))
                                        {
                                            Body = "recipient_id=" + PayGateReq.RequestUserdetails.recipient_id + "&amount=" + Amount + "&timestamp=" + timeStamp + "&currency=" + Currency + "&customer_id=" + PayGateReq.RequestUserdetails.CustomerMobile + "&initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&client_ref_id=" + uniqueclientrefid + "&hold_timeout=100&split_tid=" + splitId + "&state=" + State + "&channel=" + Channel + "&merchant_document_id_type=1&ifsc=" + PayGateReq.RequestUserdetails.Ifsc + "&merchant_document_id=ASVPV2208C&latlong=26.8863786,75.7393589&pincode=302021&user_code=" + Session["UserCode"].ToString() + "";
                                        }
                                        else
                                        {
                                            Body = "recipient_id=" + PayGateReq.RequestUserdetails.recipient_id + "&amount=" + Amount + "&timestamp=" + timeStamp + "&currency=" + Currency + "&customer_id=" + PayGateReq.RequestUserdetails.CustomerMobile + "&initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&client_ref_id=" + uniqueclientrefid + "&hold_timeout=100&split_tid=" + splitId + "&state=" + State + "&channel=" + Channel + "&merchant_document_id_type=1&merchant_document_id=ASVPV2208C&latlong=26.8863786,75.7393589&pincode=302021&user_code=" + Session["UserCode"].ToString() + "";
                                        }
                                        try
                                        {
                                            //AgencyDs = objDA.GetAgencyDetails(Session["UID"].ToString());
                                            //if (Convert.ToDouble(AgencyDs.Tables[0].Rows[0]["Crd_Limit"].ToString().Trim()) > Amount)
                                            //{

                                            debitledger(Session["UID"].ToString(), Amount, Convert.ToString(TransactionID), Convert.ToString(PayGateReq.RequestUserdetails.CustomerMobile));
                                            //}
                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                        string SendMoney = Fetch.GetCustomer(SendMoneyURL, Body, "POST", "SendMoney_SplitId", TransactionID);
                                        trnsResponse = ser.Deserialize<TransResponse>(SendMoney);
                                        //Log.Add("SendMoney_Req", SendMoneyURL);
                                        //Log.Add("SendMoney_Resp", SendMoney);
                                        if (trnsResponse.status != 0)
                                        {
                                            chktrns = false;
                                        }
                                        else if (trnsResponse.status == 0)
                                        {
                                            TransSummary trans = new TransSummary()
                                            {
                                                Status = trnsResponse.data.txstatus_desc,
                                                TID = trnsResponse.data.tid,
                                                Amount = trnsResponse.data.amount,
                                                Commission = trnsResponse.data.commission,
                                                //TDS = trnsResponse.commission,
                                                ModeOfPayment = trnsResponse.data.channel_desc,
                                                Client_Ref_Id = trnsResponse.data.client_ref_id,
                                                Customer_Id = trnsResponse.data.customer_id,
                                                Debited_User_No = trnsResponse.data.debit_user_id,
                                                Recipient_Id = trnsResponse.data.recipient_id,
                                                SplitId = splitId,
                                                Bank = trnsResponse.data.bank,
                                                Recipient_Name = trnsResponse.data.recipient_name
                                            };
                                            Transsummary.Add(trans);
                                        }
										
										if (trnsResponse.status >= 0)
										{
											if (trnsResponse.data != null)
											{
												if (!string.IsNullOrEmpty(trnsResponse.data.tid))
												{
													SqlTransaction sqlTrans = new SqlTransaction();
													//int isUpdated = sqlTrans.UpdateDmtAccountIfscCustDetail(lbl_BeneAccount.Text, lbl_BeneIfsc.Text, (lbl_CustNAME.Text + "_" + lbl_CustMobileNo.Text), trnsResponse.data.tid);
													int isUpdated = sqlTrans.UpdateDmtAccountIfscCustDetail(lbl_BeneAccount.Text, lbl_BeneIfsc.Text, (lbl_CustNAME.Text + "_" + lbl_CustMobileNo.Text), Session["AgencyName"].ToString(), Session["UID"].ToString(), Session["AgencyId"].ToString(), trnsResponse.data.tid);
												}
											}
										}
                                    }
                                }
                            }
                            div_PaymenTResult.Visible = true;
                            lbl_trnsMessage.Text = trnsResponse.message;
                            //GridView2.DataSource = Transsummary;
                            //GridView2.DataBind();
                        }
                        else
                        {
                            string SendMoneyURL = "" + serviceUrl.SendMoney + "";
                            if (Channel.Contains("1"))
                            {
                                Body = "recipient_id=" + PayGateReq.RequestUserdetails.recipient_id + "&amount=" + Amount + "&timestamp=" + timeStamp + "&currency=" + Currency + "&customer_id=" + PayGateReq.RequestUserdetails.CustomerMobile + "&initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&client_ref_id=" + uniqueclientrefid + "&hold_timeout=100&state=" + State + "&channel=" + Channel + "&merchant_document_id_type=1&ifsc=" + PayGateReq.RequestUserdetails.Ifsc + "&merchant_document_id=ASVPV2208C&latlong=26.8863786,75.7393589&pincode=302021&user_code=" + Session["UserCode"].ToString() + "";
                            }
                            else
                            {
                                Body = "recipient_id=" + PayGateReq.RequestUserdetails.recipient_id + "&amount=" + Amount + "&timestamp=" + timeStamp + "&currency=" + Currency + "&customer_id=" + PayGateReq.RequestUserdetails.CustomerMobile + "&initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&client_ref_id=" + uniqueclientrefid + "&hold_timeout=100&state=" + State + "&channel=" + Channel + "&merchant_document_id_type=1&merchant_document_id=ASVPV2208C&latlong=26.8863786,75.7393589&pincode=302021&user_code=" + Session["UserCode"].ToString() + "";
                            }

                            try
                            {

                                debitledger(Session["UID"].ToString(), Amount, Convert.ToString(TransactionID), Convert.ToString(PayGateReq.RequestUserdetails.CustomerMobile));

                            }
                            catch (Exception ex)
                            {
                                LogWriter.ErrorLog(ex.Message.ToString(), "btn_SendMoneyByChannel_Click", Convert.ToInt64(TransactionID));
                            }
                            string SendMoney = Fetch.GetCustomer(SendMoneyURL, Body, "POST", "SendMoney", TransactionID);
                            trnsResponse = ser.Deserialize<TransResponse>(SendMoney);
                            //Log.Add("SendMoney_Req", SendMoneyURL);
                            //Log.Add("SendMoney_Resp", SendMoney);
                            if (trnsResponse.status == 0)
                            {
                                TransSummary trans = new TransSummary()
                                {
                                    Status = trnsResponse.data.txstatus_desc,
                                    TID = trnsResponse.data.tid,
                                    Amount = trnsResponse.data.amount,
                                    Commission = trnsResponse.data.commission,
                                    TDS = trnsResponse.data.tds,
                                    ModeOfPayment = trnsResponse.data.channel_desc,
                                    Client_Ref_Id = trnsResponse.data.client_ref_id,
                                    Customer_Id = trnsResponse.data.customer_id,
                                    Debited_User_No = trnsResponse.data.debit_user_id,
                                    Recipient_Id = trnsResponse.data.recipient_id,
                                    SplitId = splitId,
                                    Bank = trnsResponse.data.bank,
                                    Recipient_Name = trnsResponse.data.recipient_name
                                };
                                Transsummary.Add(trans);
                            }
							
							if (trnsResponse.status >= 0)
							{
								if (trnsResponse.data != null)
								{
									if (!string.IsNullOrEmpty(trnsResponse.data.tid))
									{
										SqlTransaction sqlTrans = new SqlTransaction();
										//int isUpdated = sqlTrans.UpdateDmtAccountIfscCustDetail(lbl_BeneAccount.Text, lbl_BeneIfsc.Text, (lbl_CustNAME.Text + "_" + lbl_CustMobileNo.Text), trnsResponse.data.tid);
										int isUpdated = sqlTrans.UpdateDmtAccountIfscCustDetail(lbl_BeneAccount.Text, lbl_BeneIfsc.Text, (lbl_CustNAME.Text + "_" + lbl_CustMobileNo.Text), Session["AgencyName"].ToString(), Session["UID"].ToString(), Session["AgencyId"].ToString(), trnsResponse.data.tid);
									}
								}
							}
							
                            div_PaymenTResult.Visible = true;
                            lbl_trnsMessage.Text = trnsResponse.message;

                        }
                        if (trnsResponse.status == 0 && trnsResponse.data.tx_status == "2")
                        {
                            btn_TransStatus.Visible = true;
                            if (Channel == "2")
                            { lbl_trnsMessage.Text = "Transaction is successfully processed but the final response is awaited"; }
                            else { lbl_trnsMessage.Text = "Transaction is successfully posted"; }
                            btn_TransStatus.Visible = true;
                        }
                        btn_continueTran.Visible = true;
                        GridView2.DataSource = Transsummary;
                        GridView2.DataBind();

                        for (int i = 0; i < Transsummary.Count; i++)
                        { dtLayer.InsertTransactionData(Transsummary[i], TransactionID); }
                        if (trnsResponse.status == 0)
                        {
                            div_BenefList.Visible = true;
                            div_ReceDetails.Visible = true;
                            lbl_BeneName.Text = trnsResponse.data.recipient_name;
                            lbl_BeneBank.Text = trnsResponse.data.bank;
                            lbl_BeneAccount.Text = trnsResponse.data.account;
                            div_CustomerInfo.Visible = true;
                            div_PaymentChannel.Visible = false;
                        }
                        else if (trnsResponse.status == 313 || trnsResponse.status == 314)
                        {
                            Rdo_Channel.ClearSelection();
                            lbl_trnsMessage.Visible = false;
                            lbl_TransResponse.Text = trnsResponse.message;
                        }
                        else
                        {
                            Rdo_Channel.ClearSelection();
                            lbl_TransResponse.Text = trnsResponse.message;
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Agency Do not have enough balance');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Minimum Amount Rs.500');", true);
            }
        }
        catch (Exception ex)
        { LogWriter.ErrorLog(ex.Message.ToString(), "btn_SendMoneyByChannel_Click", Convert.ToInt64(TransactionID)); }
    }

    private string GetLatLon()
    {
        return "";
        //try
        //{
        //    double latitude = 0, logitude = 0;
        //    GeoCoordinateWatcher watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.Default);
        //    watcher.Start(); //started watcher
        //    GeoCoordinate coord = watcher.Position.Location;
        //    if (!watcher.Position.Location.IsUnknown)
        //    {
        //        latitude = coord.Latitude; //latitude
        //        logitude = coord.Longitude;  //logitude

        //    }
        //    return latitude + "," + logitude;
        //}
        //catch (Exception ex)
        //{
        //    return 0 + "," + 0;
        //    LogWriter.ErrorLog(ex.Message.ToString(), "GetLatLon", Convert.ToInt16(TransactionID));
        //}
    }



    public static String GetTimestamp(DateTime value)
    {
        return value.ToString(@"yyyy-MM-dd HH\:mm\:ss");
    }

    protected void btn_Refund_Click(object sender, EventArgs e)
    {
        btn_Refund.Visible = false;
        btn_SendMoney.Visible = false;
        div_RefndDetails.Visible = true;
        div_start.Visible = false;
        //div_ResendOtp.Visible = true;
        //btn_Proceed.Visible = false;

    }

    protected void btn_InitateRefund_Click(object sender, EventArgs e)
    {
        try
        {
            lbl_iSTransIdValid.Text = "";
            lbl_iSTransIdValid.Visible = true;
            string TransIds = TrnsId.Value;
            DataTable dttid = new DataTable();
            //Int64 otp = Convert.ToInt64(OTP.Value);
            dttid = getmobilenumber(TransIds);
            dtgetdetail = getuserid(Session["UID"].ToString());//getuserid(ViewState["mobilenumber"].ToString());

            List<RefundSummary2> Refundsummary2 = new List<RefundSummary2>();
            if (TransIds != null)
            {
                SequrityKeys ks = Eko.GetSequrityKeys;
                PayGateReq.RequestUserdetails.Secrect_Key = ks.secret_key;
                PayGateReq.RequestUserdetails.secretKeyTimestamp = ks.secret_key_timestamp;
                string ChkIfTransStatusIsRefundURL = "" + serviceUrl.ChkIfInRefund + "" + TransIds + "?initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&user_code=" + Session["UserCode"].ToString() + "";
                string ChkIfTransStatusIsRefundResp = Fetch.GetCustomer(ChkIfTransStatusIsRefundURL, "", "GET", "ChkIfTransIsInRefundState", TransactionID);
                ChkIfTransStatusIsRefund = ser.Deserialize<TransStatus>(ChkIfTransStatusIsRefundResp);
                //  Log.Add("ChkIfTransStatusIsRefund_Req", ChkIfTransStatusIsRefundURL);
                //  Log.Add("ChkIfTransStatusIsRefund_Resp", ChkIfTransStatusIsRefundResp);
                if (ChkIfTransStatusIsRefund != null)
                {
                    //if (ChkIfTransStatusIsRefund.data.txstatus_desc.Contains("Success") && ChkIfTransStatusIsRefund.status == 0 && ChkIfTransStatusIsRefund.data.tx_status == "0")
                    if (ChkIfTransStatusIsRefund.status == 0 && !ChkIfTransStatusIsRefund.data.txstatus_desc.Contains("Refund Pending"))
                    {
                        RefundSummary2 trans = new RefundSummary2()
                        {
                            TID = ChkIfTransStatusIsRefund.data.tid,
                            Amount = ChkIfTransStatusIsRefund.data.amount,
                            Status = ChkIfTransStatusIsRefund.data.txstatus_desc,
                            Type = ChkIfTransStatusIsRefund.data.tx_desc,
                            TransactionTime = Convert.ToString(ChkIfTransStatusIsRefund.data.timestamp)
                        };
                        Refundsummary2.Add(trans);
                        lbl_RefundStatus2.Text = RefundEvalue.message;
                        div_grid4.Visible = true;
                        GridView4.DataSource = Refundsummary2;
                        GridView4.DataBind();
                        lbl_RefundStatus2.Text = "Transaction was succesful.";
                        div_RefndDetails.Visible = false;
                    }
                    else if (ChkIfTransStatusIsRefund.status == 69)
                    { lbl_iSTransIdValid.Text = "Please provide a valid TID to know the status of the transaction."; }
                    else
                    {
                        div_Otp.Visible = true;
                        div_ResendOtp.Visible = true;
                        btn_Proceed.Visible = false;
						 div_RefndDetails.Visible = false;
                        div_Refund.Visible = true;
                        verfiacc.Visible = true;

                    }
                }
                else
                {
                    lbl_RefundStatus.Text = "Soething Went Wrong.There was no response from server";
                }

            }
        }
        catch (Exception ex)
        { LogWriter.ErrorLog(ex.Message.ToString(), "btn_InitateRefund_Click", Convert.ToInt64(TransactionID)); }
    }


    protected void btn_Initiate_Click(object sender, EventArgs e)
    {
        try
        {
            string TransIds = TrnsId.Value;
            Int64 otp = Convert.ToInt64(OTP.Value);
            List<RefundSummary> Refundsummary = new List<RefundSummary>();
            SequrityKeys ks = Eko.GetSequrityKeys;
            PayGateReq.RequestUserdetails.Secrect_Key = ks.secret_key;
            PayGateReq.RequestUserdetails.secretKeyTimestamp = ks.secret_key_timestamp;
            DataTable dttid = new DataTable();
            dttid = getmobilenumber(TransIds);
            dtgetdetail = getuserid(Session["UID"].ToString());//getuserid(ViewState["mobilenumber"].ToString());

            string RefundMoneyURL = "" + serviceUrl.ChkIfInRefund + "" + TransIds + "/refund";
            string body = "initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&otp=" + otp + "&state=1&user_code=" + Session["UserCode"].ToString() + "";
            string RefundResp = Fetch.GetCustomer(RefundMoneyURL, body, "POST", "RefundMoney", TransactionID);
            RefundEvalue = ser.Deserialize<RefundEvalue>(RefundResp);
            //  Log.Add("RefundMoney_Req", RefundMoneyURL);
            //  Log.Add("RefundMoney_Resp", RefundResp);
            if (RefundEvalue.status == 0) //Here tx_status should be 4 is to be checked.
            {

                RefundSummary trans = new RefundSummary()
                {
                    TID = RefundEvalue.data.tid,
                    Refunded_Amount = RefundEvalue.data.amount,
                    Commission_Reverse = RefundEvalue.data.commission_reverse,
                    TDS = RefundEvalue.data.tds
                };
                Refundsummary.Add(trans);
                creditledger(Session["UID"].ToString(), Convert.ToDouble(trans.Refunded_Amount), Convert.ToString(TransactionID), Convert.ToString(PayGateReq.RequestUserdetails.CustomerMobile));
                dtLayer.InsertRefundData(Refundsummary[0], TransactionID);
                div_grid3.Visible = true;
                lbl_RefundStatus.Text = RefundEvalue.message;
                GridView3.DataSource = Refundsummary;
                GridView3.DataBind();
                lbl_RefundStatus.Text = "Refund Successful";
                div_RefndDetails.Visible = false;
                div_Otp.Visible = false;
                div_ResendOtp.Visible = false;
                btn_Proceed.Visible = false;
				 div_RefndDetails.Visible = false;
                div_Refund.Visible = false;
                verfiacc.Visible = false;
            }
            else
            {
                lbl_RefundStatus.Text = RefundEvalue.message;
            }
        }
        catch (Exception ex)
        {
            LogWriter.ErrorLog(ex.Message.ToString(), "btn_Initiate_Click", Convert.ToInt64(TransactionID));
        }
    }

    protected void ResendRfn_OTP_Click(object sender, EventArgs e)
    {
        try
        {
            string TransIds = TrnsId.Value;
            SequrityKeys ks = Eko.GetSequrityKeys;
            PayGateReq.RequestUserdetails.Secrect_Key = ks.secret_key;
            PayGateReq.RequestUserdetails.secretKeyTimestamp = ks.secret_key_timestamp;
            DataTable dttid = new DataTable();
            dttid = getmobilenumber(TransIds);
            dtgetdetail = getuserid(Session["UID"].ToString());//getuserid(ViewState["mobilenumber"].ToString());
            string RefundOtpURL = "" + serviceUrl.GetRefund_OTP + "" + TransIds + "/refund/otp";
            //string body = "initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id+"& user_code = "+UserCode+"";
            string body = "initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "";
            string RefundOTPResp = Fetch.GetCustomer(RefundOtpURL, body, "POST", "ResendRfndOTP", TransactionID);
            GetRefundOTP = ser.Deserialize<ResendRefundOTP>(RefundOTPResp);
            // Log.Add("RefundOTP_Req", RefundOtpURL);
            //  Log.Add("RefundOTP_Resp", RefundOTPResp);
            if (GetRefundOTP != null)
            {
                if (GetRefundOTP.status == 0)
                {
                    OTP.Value = GetRefundOTP.data.otp;
                    lbl_ResendRefund_otp.Text = "OTP request has been succesfully sent.";
                    //div_Otp.Visible = true;
                    //ResendRfn_OTP.Visible = false;
                    //div_Refund.Visible = true;
                }
                else if (GetRefundOTP.status == 168)
                {
                    lbl_ResendRefund_otp.Text = GetRefundOTP.message;
                }
            }
            else { lbl_ResendRefund_otp.Text = "Something Went Wrong. There was no response from Server."; }

        }
        catch (Exception ex)
        { LogWriter.ErrorLog(ex.Message.ToString(), "ResendRfn_OTP_Click", Convert.ToInt64(TransactionID)); }
    }

    protected void btngetdetails_Click(object sender, EventArgs e)
    {
        try
        {
            btn_DeleteRecipients.Visible = false;
            string BeneName = string.Empty;
            string BeneIfsc = string.Empty;
            string BeneRecepient_Id = string.Empty;
            Int64 BeneAccount = 0;
            string BenefBank = string.Empty;
            DataTable dt = new DataTable();
            dtgetdetail = getuserid(Session["UID"].ToString());
            dt.Columns.AddRange(new DataColumn[5] { new DataColumn("name"), new DataColumn("Bank"), new DataColumn("Account"), new DataColumn("Ifsc"), new DataColumn("recipient_id") });
            foreach (GridViewRow row in GridView1.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    RadioButton rb = (RadioButton)row.Cells[0].FindControl("RadioButton1");
                    if (rb.Checked)
                    {
                        BeneName = (row.Cells[1].Text);
                        BenefBank = (row.Cells[2].Text);
                        BeneAccount = Int64.Parse(row.Cells[3].Text);
                        BeneIfsc = row.Cells[4].Text;
                        BeneRecepient_Id = row.Cells[5].Text;
                        dt.Rows.Add(BeneName, BenefBank, BeneAccount, BeneIfsc, BeneRecepient_Id);
                        break;
                    }
                }
            }
            if (dt.Rows.Count > 0)
            {
                PayGateReq.RequestUserdetails.Ifsc = dt.Rows[0][3].ToString();
                PayGateReq.RequestUserdetails.recipient_id = dt.Rows[0][4].ToString();
                string BankURL = "" + serviceUrl.BankData_Ifsc + "" + PayGateReq.RequestUserdetails.Ifsc + "&initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&user_code=" + Session["UserCode"].ToString() + "";
                string BankResp = Fetch.GetCustomer(BankURL, "", "GET", "BankDetails", TransactionID);
                BankResponse = ser.Deserialize<BankResponse>(BankResp);
                //Log.Add("BankDetails_Req", BankURL);
                //Log.Add("BankDetails_Resp", BankResp);
                div_CustomerInfo.Visible = true;
                div_ReceDetails.Visible = true;
                GridView1.Visible = false;
                div_Payment.Visible = true;
                div_PaymentChannel.Visible = true;
                Button1.Visible = false;
                btngetdetails.Visible = false;
                div_Cancel.Visible = true;
                lbl_BeneName.Text = BeneName;
                lbl_BeneBank.Text = BankResponse.data.bank;
                lbl_BeneAccount.Text = Convert.ToString(BeneAccount);
                lbl_BeneIfsc.Text = BankResponse.data.ifsc;
                if (BankResponse.data.available_channels == 0)
                {
                    div_Payment.Visible = true;
                    Rdo_Channel.Visible = true;
                }
                else if (BankResponse.data.available_channels == 1)
                {
                    div_Payment.Visible = true;
                    Rdo_Channel.Visible = true;
                    Rdo_Channel.Items.Remove(Rdo_Channel.Items.FindByValue("2"));
                }
                else
                {
                    div_Payment.Visible = true;
                    Rdo_Channel.Visible = true;
                    Rdo_Channel.Items.Remove(Rdo_Channel.Items.FindByValue("1"));
                }
            }
            else
            {
                btn_DeleteRecipients.Visible = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please select Beneficary to start payment');", true);
            }
        }
        catch (Exception ex)
        {
            btn_DeleteRecipients.Visible = true;
            LogWriter.ErrorLog(ex.Message.ToString(), "btngetdetails_Click", Convert.ToInt64(TransactionID));
        }
    }

    protected void btn_RegisterCust_Click(object sender, EventArgs e)
    {
        try
        {

            //string RegisterCustURL = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile;
            //string Body = "initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&name=" + PayGateReq.RequestUserdetails.NewCustomer + "";
            //string RegisterCustResponse = Fetch.GetCustomer(RegisterCustURL, Body, "PUT", "RegisterNewCust", TransactionID);
            dtgetdetail = getuserid(Session["UID"].ToString());
            string URlforpipe = "" + serviceUrl.getcustomer + "mobile_number:" + PayGateReq.RequestUserdetails.CustomerMobile + "";
            string body = "initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&user_code=" + Session["UserCode"].ToString() + "&pipe=9&name=" + dtgetdetail.Rows[0]["FirstName"].ToString() + "&residence_address={\"line\":\"EkoIndia\",\"city\":\"" + dtgetdetail.Rows[0]["City"].ToString() + "\",\"state\":\"" + dtgetdetail.Rows[0]["State"].ToString() + "\",\"pincode\":\"" + dtgetdetail.Rows[0]["Pincode"].ToString() + "\",\"district\":\"" + dtgetdetail.Rows[0]["District"].ToString() + "\",\"area\":\"" + dtgetdetail.Rows[0]["Area"].ToString() + "\"}&dob=" + dtgetdetail.Rows[0]["DOB"].ToString() + "";
            string RegisterCustResponse = Fetch.GetCustomer(URlforpipe, body, "PUT", "RegisterNewCust", TransactionID);
            NewCustomerResponse = ser.Deserialize<CreateCustomer>(RegisterCustResponse);
            if (RegisterCustResponse.Contains("OTP has been sent") || RegisterCustResponse.Contains("Verify customer using OTP"))
            {
                int i = insertotpforcustomer(NewCustomerResponse.data.user_code, NewCustomerResponse.data.customer_id, NewCustomerResponse.data.otp_ref_id, NewCustomerResponse.response_type_id, NewCustomerResponse.message);
            }


            //   Log.Add("RegisterCust_Req", RegisterCustURL);
            //  Log.Add("RegisterCust_Resp", RegisterCustResponse);
            if (NewCustomerResponse.status == 0)
            {
                div_btn.Visible = false;

                div_no.Visible = false;
                div_EnterOTP.Visible = true;
                txt_OTP.Value = NewCustomerResponse.data.otp;
                lbl_Verify.Text = NewCustomerResponse.message;
            }
            else if (NewCustomerResponse.status == 17 || NewCustomerResponse.message.Contains("Sender already registered"))
            {
                string Url = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile + "?initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "";
                string CustomerResp = Fetch.GetCustomer(Url, "", "GET", "", TransactionID);
                CustomerResponse = ser.Deserialize<GetCustmerInfo>(CustomerResp);

                if (CustomerResponse.status == 0 || CustomerResponse.message.Contains("Non-KYC active") || CustomerResponse.message.Contains("Sender already registered"))
                {
                    lbl_BankResponse.Visible = false;
                    string urlgetrecip = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile + "/recipients?initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "";
                    string receipentsResp = Fetch.GetCustomer(urlgetrecip, "", "GET", "GetCustomerBenefeciaries", TransactionID);
                    receipents = ser.Deserialize<GetAllRecepients>(receipentsResp);
                    bindvalue();
                }
            }
        }
        catch (Exception ex)
        { LogWriter.ErrorLog(ex.Message.ToString(), "btn_RegisterCust_Click", Convert.ToInt64(TransactionID)); }
    }

    protected void BankName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //lbl_RecepientCreated.Style.Add(HtmlTextWriterStyle.Display, "none");
            lbl_RecepientCreated.Visible = false;
            lbl_isIfscRequired.Visible = true;
            string BankName = BankList.SelectedItem.Text;
            dtgetdetail = getuserid(Session["UID"].ToString());
            if (BankName != "Please Select Bank")
            {
                string BankCode = BankList.SelectedValue;
                BankCode = BankCode.Replace(" ", String.Empty);

                string BankURL = "" + serviceUrl.BankData_BankCode + "" + BankCode + "&initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&user_code=" + Session["UserCode"].ToString() + "";
                string BankResp = Fetch.GetCustomer(BankURL, "", "GET", "ChkValidation&IfscStatus", TransactionID);
                BankResponse = ser.Deserialize<BankResponse>(BankResp);
                // Log.Add("BankSelctionChng_Req", BankURL);
                //  Log.Add("BankSelctionChng_Resp", BankResp);
                if (BankResponse.data.ifsc_status != 4)
                {
                    lbl_isIfscRequired.Text = "Optional";
                }
                else { lbl_isIfscRequired.Text = "Required"; }
                if (BankResponse.data.isverificationavailable == "1")
                {
                    //div1.Style.Add(HtmlTextWriterStyle.Display, "none");
                    div1.Visible = false;
                    //div_beneNameAndMobile.Style.Add(HtmlTextWriterStyle.Display, "none");
                    div_beneNameAndMobile.Visible = false;
                    div_Validate.Visible = true;
                }
                else
                {
                    lbl_isIfscRequired.Visible = false;
                    div_Validate.Visible = false;
                    //div_beneNameAndMobile.Style.Add(HtmlTextWriterStyle.Display, "block");
                    div_beneNameAndMobile.Visible = true;
                    //div1.Style.Add(HtmlTextWriterStyle.Display, "none");
                    div1.Visible = true;
                }
            }
            else
            {
                benef_Accountno.Value = "";
                benef_mobile.Value = "";
                benef_ifsc.Value = "";
                benef_name.Value = "";
                lbl_isIfscRequired.Text = "";
                //Add_Recepients.Style.Add(HtmlTextWriterStyle.Display, "block");
                Add_Recepients.Visible = true;
                btn_cancel.Visible = true;
                div_Validate.Visible = false;
                //div1.Style.Add(HtmlTextWriterStyle.Display, "none");
                div1.Visible = false;
                //div_beneNameAndMobile.Style.Add(HtmlTextWriterStyle.Display, "none");
                div_beneNameAndMobile.Visible = false;
            }
        }
        catch (Exception ex)
        { LogWriter.ErrorLog(ex.Message.ToString(), "BankName_SelectedIndexChanged", Convert.ToInt64(TransactionID)); }
    }

    protected void btn_DeleteRecipients_Click(object sender, EventArgs e)
    {
        try
        {
            dtgetdetail = getuserid(Session["UID"].ToString());
            string RecipientId = string.Empty;
            Int64 BeneAccount = 0;
            foreach (GridViewRow row in GridView1.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    RadioButton rb = (RadioButton)row.Cells[0].FindControl("RadioButton1");
                    if (rb.Checked)
                    {
                        BeneAccount = Int64.Parse(row.Cells[3].Text);
                        string BeneIfsc = row.Cells[4].Text;
                        RecipientId = row.Cells[5].Text;
                        break;
                        //dt.Rows.Add(id, BeneIfsc);
                    }
                }
            }
            if (BeneAccount != 0 && RecipientId != "")
            {
                string DeleteBenefURL = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile + "/recipients/recipient_id:" + RecipientId + "";
                string body = "initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&user_code=" + Session["UserCode"].ToString() + "";
                string DeleteBenefResp = Fetch.GetCustomer(DeleteBenefURL, body, "DELETE", "DeleteRecipient", TransactionID);
                isReipientDelted = ser.Deserialize<RecipientDeleted>(DeleteBenefResp);

                // DeletedLog.Add("DeletedBenef_Req", DeleteBenefURL);
                // DeletedLog.Add("DeletedBenef_Resp", DeleteBenefResp);

                if (isReipientDelted.status == 0 && isReipientDelted.message.Contains("DELETED"))
                {
                    string Url = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile + "?initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&user_code=" + Session["UserCode"].ToString() + "";
                    string CustomerResp = Fetch.GetCustomer(Url, "", "GET", "", TransactionID);
                    CustomerResponse = ser.Deserialize<GetCustmerInfo>(CustomerResp);
                    if (CustomerResponse.status == 0 || CustomerResponse.message.Contains("Non-KYC active") || CustomerResponse.message.Contains("Sender already registered"))
                    {
                        lbl_BankResponse.Visible = false;
                        string urlgetrecip = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile + "/recipients?initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "&user_code=" + Session["UserCode"].ToString() + "";
                        string receipentsResp = Fetch.GetCustomer(urlgetrecip, "", "GET", "GetCustomerBenefeciaries", TransactionID);
                        receipents = ser.Deserialize<GetAllRecepients>(receipentsResp);
                        bindvalue();
                    }

                }
            }
            else
            { ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please select Beneficary to delete.');", true); }
        }
        catch (Exception ex)
        { LogWriter.ErrorLog(ex.Message.ToString(), "btn_DeleteRecipients_Click", Convert.ToInt64(TransactionID)); }
    }

    protected void btn_TransStatus_Click(object sender, EventArgs e)
    {
        string ModeOfPayment = string.Empty;
        bool flag = true;
        try
        {
            List<TransSummary> ChngTranStataus = new List<TransSummary>();
            if (Transsummary.Count > 0)
            {
                for (int i = 0; i < Transsummary.Count; i++)
                {
                    string TransStatusURL = "" + serviceUrl.TransEnquiry + "" + Transsummary[i].TID + "?initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "";
                    string TransStatusResp = Fetch.GetCustomer(TransStatusURL, "", "GET", "TransactionEnquiry", TransactionID);
                    TransAwaitedStatus = ser.Deserialize<TransAwaited>(TransStatusResp);
                    if (TransAwaitedStatus.data.channel == "1")
                    { ModeOfPayment = "NEFT"; }
                    else { ModeOfPayment = "IMPS"; }
                    TransSummary trans = new TransSummary()
                    {
                        Status = TransAwaitedStatus.data.txstatus_desc,
                        TID = TransAwaitedStatus.data.tid,
                        TDS = TransAwaitedStatus.data.tds,
                        Amount = TransAwaitedStatus.data.amount,
                        Commission = TransAwaitedStatus.data.commission,
                        ModeOfPayment = ModeOfPayment,
                        Client_Ref_Id = TransAwaitedStatus.data.client_ref_id,
                        Customer_Id = TransAwaitedStatus.data.customer_id,
                        //Debited_User_No = trnsResponse.debit_user_id,
                        Recipient_Id = TransAwaitedStatus.data.recipient_id,
                        //SplitId = splitId,
                        //Bank = trnsResponse.bank,
                        //Recipient_Name = trnsResponse.recipient_name
                    };
                    ChngTranStataus.Add(trans);
                }
                for (int i = 0; i < ChngTranStataus.Count; i++)
                {
                    if (ChngTranStataus[i].Status == "Response Awaited" || ChngTranStataus[i].Status == "Initiated")
                    {
                        flag = false;
                        btn_TransStatus.Visible = true;
                    }
                }
                if (flag)
                { btn_TransStatus.Visible = false; }
                GridView2.DataSource = ChngTranStataus;
                GridView2.DataBind();
            }
        }
        catch (Exception ex)
        { LogWriter.ErrorLog(ex.Message.ToString(), "btn_TransStatus_Click", Convert.ToInt64(TransactionID)); }
    }

    protected void btn_continueTran_Click(object sender, EventArgs e)
    {
        div_PaymenTResult.Visible = false;
        lbl_trnsMessage.Visible = false;
        div_Payment.Visible = false;

        string Url = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile + "?initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "";
        string CustomerResp = Fetch.GetCustomer(Url, "", "GET", "GetCustomerInfo", TransactionID);
        CustomerResponse = ser.Deserialize<GetCustmerInfo>(CustomerResp);
        if (CustomerResponse.status == 0 && CustomerResponse.message.Contains("Non-KYC active") || CustomerResponse.message.Contains("Sender already registered"))
        {
            string urlgetrecip = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile + "/recipients?initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "";
            string receipentsResp = Fetch.GetCustomer(urlgetrecip, "", "GET", "GetCustomerBenefeciaries", TransactionID);
            receipents = ser.Deserialize<GetAllRecepients>(receipentsResp);
            //    Log.Add("GetCustBenef_Req", urlgetrecip);
            //   Log.Add("GetCustBenef_Resp", receipentsResp);
            if (receipents.message.Contains("No recepients found"))
            {
                div_CustomerInfo.Visible = true;
                lbl_CustNAME.Text = Convert.ToString(CustomerResponse.data.name);
                lbl_CustMobileNo.Text = Convert.ToString(CustomerResponse.data.mobile);
                total_limit.Text = Convert.ToString(CustomerResponse.data.total_limit);
                aval_limit.Text = Convert.ToString(CustomerResponse.data.available_limit);
                btngetdetails.Visible = true;//By Default False
                btn_DeleteRecipients.Visible = false;
                div_no.Visible = false;
                Button1.Visible = true;

            }
            else if (receipents.data != null)
            {
                Button1.Visible = true;
                btn_DeleteRecipients.Visible = true;
                btngetdetails.Visible = true;
                bindvalue();
            }
        }
    }

    protected void btn_Reset_Click(object sender, EventArgs e)
    {
        div_RefndDetails.Visible = true;
        TrnsId.Value = "";
        OTP.Value = "";
        lbl_ResendRefund_otp.Text = "";
        div_grid4.Visible = false;
        div_grid3.Visible = false;
        div_Otp.Visible = false;
        div_ResendOtp.Visible = false;
        btn_Proceed.Visible = true;
		 div_RefndDetails.Visible = true;
        div_Refund.Visible = false;
        verfiacc.Visible = false;
    }

    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        //Add_Recepients.Style.Add(HtmlTextWriterStyle.Display, "none");
        Add_Recepients.Visible = false;
        lbl_isIfscRequired.Text = "";
        div_Cancel.Visible = false;
        // div_Cancel2.Visible = false;
        div_ReceDetails.Visible = false;
        div_beneNameAndMobile.Visible = false;
        div_PaymentChannel.Visible = false;
        string Url = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile + "?initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "";
        string CustomerResp = Fetch.GetCustomer(Url, "", "GET", "GetCustomerInfo", TransactionID);
        CustomerResponse = ser.Deserialize<GetCustmerInfo>(CustomerResp);
        if (CustomerResponse.status == 0 && CustomerResponse.message.Contains("Non-KYC active") || CustomerResponse.message.Contains("Sender already registered"))
        {
            string urlgetrecip = "" + serviceUrl.getcustomer + "" + PayGateReq.RequestUserdetails.customer_id_type + ":" + PayGateReq.RequestUserdetails.CustomerMobile + "/recipients?initiator_id=" + PayGateReq.RequestUserdetails.Initatior_Id + "";
            string receipentsResp = Fetch.GetCustomer(urlgetrecip, "", "GET", "GetCustomerBenefeciaries", TransactionID);
            receipents = ser.Deserialize<GetAllRecepients>(receipentsResp);
            //   Log.Add("GetCustBenef_Req", urlgetrecip);
            //   Log.Add("GetCustBenef_Resp", receipentsResp);
            if (receipents.message.Contains("No recepients found"))
            {
                div_CustomerInfo.Visible = true;
                lbl_CustNAME.Text = Convert.ToString(CustomerResponse.data.name);
                lbl_CustMobileNo.Text = Convert.ToString(CustomerResponse.data.mobile);
                total_limit.Text = Convert.ToString(CustomerResponse.data.total_limit);
                aval_limit.Text = Convert.ToString(CustomerResponse.data.available_limit);
                btngetdetails.Visible = true;//by default false
                btn_DeleteRecipients.Visible = false;
                div_no.Visible = false;
                Button1.Visible = true;

            }
            else if (receipents.data != null)
            {
                Button1.Visible = true;
                btn_DeleteRecipients.Visible = true;
                btngetdetails.Visible = true;
                bindvalue();
            }
        }

    }

    public void debitledger(string Userid, double Amount, string Orderid, string id)
    {

        try
        {


            Hashtable HSDEBIT = new Hashtable();
            HSDEBIT = GET_INSERTUPLOADDETAILS_TRANSACTION(Convert.ToDouble(Amount), Userid, "", "", "", "", "", "", Userid, "", "1234", Convert.ToDouble(Amount), 0, "Debit", "DMT DEBIT " + Amount + "", 0, "DMT", "", 0, "", "", "MONEY TRANSFER", "", "", "", 0, "");

        }
        catch (Exception EX)
        {
            LogWriter.ErrorLog(EX.Message.ToString(), "debitledger", Convert.ToInt64(TransactionID));
        }
    }

    public void creditledger(string Userid, double Amount, string Orderid, string id)
    {

        try
        {
            Hashtable HSDEBIT = new Hashtable();
            HSDEBIT = GET_INSERTUPLOADDETAILS_TRANSACTION(Convert.ToDouble(Amount), Userid, "", "", "", "", "", "", Userid, "", "1234", Convert.ToDouble(Amount), 0, "Credit", "DMT DEBIT " + Amount + "", 0, "DMT", "", 0, "", "", "MONEY TRANSFER", "", "", "", 0, "");

        }
        catch (Exception EX)
        {
            LogWriter.ErrorLog(EX.Message.ToString(), "creditledger", Convert.ToInt64(TransactionID));
        }
    }


    private Hashtable GET_INSERTUPLOADDETAILS_TRANSACTION(double Amount, string AgentId, string AgencyName, string InvoiceNo, string PnrNo, string TicketNo, string TicketingCarrier, string YatraAccountID, string AccountID, string ExecutiveID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, int PaxId, string Uploadtype, string YtrRcptNo, int ID, string Status, string Type, string Rmk, string BankName, string BankCode, string Narration, int SplStatus, string TransType)
    {
        Hashtable HS = new Hashtable();
        try
        {

            SqlConnection con = new SqlConnection();
            SqlDataAdapter adp;
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            if (con.State == ConnectionState.Open)
                con.Close();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
            adp = new SqlDataAdapter("SP_INSERTUPLOADDETAILS_TRANSACTION_DMT", con);
            adp.SelectCommand.CommandType = CommandType.StoredProcedure;
            adp.SelectCommand.Parameters.AddWithValue("@Amount", Amount);
            adp.SelectCommand.Parameters.AddWithValue("@AgentId", AgentId);
            adp.SelectCommand.Parameters.AddWithValue("@AgencyName", Session["AgencyName"].ToString());
            adp.SelectCommand.Parameters.AddWithValue("@InvoiceNo", InvoiceNo);
            adp.SelectCommand.Parameters.AddWithValue("@PnrNo", PnrNo);
            adp.SelectCommand.Parameters.AddWithValue("@TicketNo", TicketNo);
            adp.SelectCommand.Parameters.AddWithValue("@TicketingCarrier", TicketingCarrier);
            adp.SelectCommand.Parameters.AddWithValue("@YatraAccountID", YatraAccountID);
            adp.SelectCommand.Parameters.AddWithValue("@AccountID", AccountID);
            adp.SelectCommand.Parameters.AddWithValue("@ExecutiveID", ExecutiveID);
            adp.SelectCommand.Parameters.AddWithValue("@IPAddress", IPAddress);
            adp.SelectCommand.Parameters.AddWithValue("@Debit", Debit);
            adp.SelectCommand.Parameters.AddWithValue("@Credit", Credit);
            adp.SelectCommand.Parameters.AddWithValue("@BookingType", BookingType);
            adp.SelectCommand.Parameters.AddWithValue("@Remark", Remark);
            adp.SelectCommand.Parameters.AddWithValue("@PaxId", PaxId);
            adp.SelectCommand.Parameters.AddWithValue("@Uploadtype", Uploadtype);
            adp.SelectCommand.Parameters.AddWithValue("@YtrRcptNo", YtrRcptNo);
            adp.SelectCommand.Parameters.AddWithValue("@ID", ID);
            adp.SelectCommand.Parameters.AddWithValue("@Status", Status);
            adp.SelectCommand.Parameters.AddWithValue("@Type", Type);
            adp.SelectCommand.Parameters.AddWithValue("@Rmk", Rmk);
            adp.SelectCommand.Parameters.AddWithValue("@SplStatus", SplStatus);
            adp.SelectCommand.Parameters.AddWithValue("@BankName", BankName);
            adp.SelectCommand.Parameters.AddWithValue("@BankCode", BankCode);
            adp.SelectCommand.Parameters.AddWithValue("@Narration", Narration);
            adp.SelectCommand.Parameters.AddWithValue("@TransType", TransType);
            adp.SelectCommand.Parameters.Add("@Aval_Balance", SqlDbType.Decimal);
            adp.SelectCommand.Parameters["@Aval_Balance"].Direction = ParameterDirection.Output;
            adp.SelectCommand.Parameters.Add("@result", SqlDbType.NVarChar, 500);
            adp.SelectCommand.Parameters["@result"].Direction = ParameterDirection.Output;
            adp.Fill(dt);
            string Result = adp.SelectCommand.Parameters["@result"].Value.ToString();
            decimal AvailableBalance = Convert.ToDecimal(adp.SelectCommand.Parameters["@Aval_Balance"].Value.ToString());
            HS.Add("RESULT", Result);
            HS.Add("AVLBALANCE", AvailableBalance);

        }
        catch (Exception ex)
        {
            LogWriter.ErrorLog(ex.Message.ToString(), "GET_INSERTUPLOADDETAILS_TRANSACTION", Convert.ToInt64(TransactionID));
        }
        return HS;
    }

    protected DataTable getuserid(string UserID)
    {
        DataTable dt = new DataTable();
        //string UserCode = "";
        try
        {

            conn.Open();
            SqlCommand cmd = new SqlCommand("Sp_GetDmtUserCode", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userid", UserID);
            SqlDataAdapter sdr = new SqlDataAdapter(cmd);

            sdr.Fill(dt);
            //if (dt.Rows.Count > 0)
            //{
            //    UserCode = dt.Rows[0]["User_Code"].ToString();
            //}
            conn.Close();

        }
        catch (Exception ex)
        {
            LogWriter.ErrorLog(ex.Message.ToString(), "getuserid", Convert.ToInt64(TransactionID));
        }
        return dt;
    }

    protected DataTable getmobilenumber(string TID)
    {
        DataTable dt = new DataTable();
        //string UserCode = "";
        try
        {

            conn.Open();
            SqlCommand cmd = new SqlCommand("Sp_GetDmtmobilenumber", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Tid", TID);
            SqlDataAdapter sdr = new SqlDataAdapter(cmd);

            sdr.Fill(dt);
            //if (dt.Rows.Count > 0)
            //{
            //    UserCode = dt.Rows[0]["User_Code"].ToString();
            //}
            conn.Close();

        }
        catch (Exception ex)
        {
            LogWriter.ErrorLog(ex.Message.ToString(), "getmobilenumber", Convert.ToInt64(TransactionID));
        }
        return dt;
    }

    protected int insertotpforcustomer(string user_code, string customer_id, string otp_ref_id, int response_type_id, string message)
    {
        int kl = 0;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        try
        {
            con.Open();
            Hashtable HS = new Hashtable();
            SqlCommand cmd = new SqlCommand("Sp_Tbl_DMT_CustumerOTP", con);
            //if (con.State == ConnectionState.Open)
            //    con.Close();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@user_code", user_code);
            cmd.Parameters.AddWithValue("@customer_id", customer_id);
            cmd.Parameters.AddWithValue("@otp_ref_id", otp_ref_id);
            cmd.Parameters.AddWithValue("@response_type_id", Convert.ToString(response_type_id));
            cmd.Parameters.AddWithValue("@message", message);
            cmd.Parameters.AddWithValue("@action", "Insert");
            kl = cmd.ExecuteNonQuery();
            con.Close();

        }
        catch (Exception ex)
        {
        }
        return kl;

    }

    protected int updateotp(string user_code, string customer_id, string OTP, string otp_ref_id)
    {
        int kl = 0;
        DataTable dtotp = new DataTable();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        try
        {
            con.Open();
            Hashtable HS = new Hashtable();
            SqlCommand cmd = new SqlCommand("Sp_Tbl_DMT_CustumerOTP", con);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@user_code", user_code);
            cmd.Parameters.AddWithValue("@customer_id", customer_id);
            cmd.Parameters.AddWithValue("@otp_ref_id", otp_ref_id);
            cmd.Parameters.AddWithValue("@OTP", OTP);
            cmd.Parameters.AddWithValue("@action", "Update");
            kl = cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        {
        }
        return kl;

    }

    protected DataTable getotpref(string customer_id)
    {
        DataTable dt = new DataTable();
        //string UserCode = "";
        try
        {

            conn.Open();
            SqlCommand cmd = new SqlCommand("Sp_Tbl_DMT_CustumerOTP", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@customer_id", customer_id);
            cmd.Parameters.AddWithValue("@action", "Select");
            SqlDataAdapter sdr = new SqlDataAdapter(cmd);

            sdr.Fill(dt);
            conn.Close();

        }
        catch (Exception ex)
        {
            LogWriter.ErrorLog(ex.Message.ToString(), "getotpref", Convert.ToInt64(TransactionID));
        }
        return dt;
    }

    public string uniquekey()
    {
        Guid g = Guid.NewGuid();
        string GuidString = Convert.ToBase64String(g.ToByteArray());
        GuidString = GuidString.Replace("=", "");
        GuidString = GuidString.Replace("+", "");
        GuidString = "RIM" + GuidString;
        GuidString = GuidString.Substring(0, 10).ToUpper();
        return GuidString;
    }
}