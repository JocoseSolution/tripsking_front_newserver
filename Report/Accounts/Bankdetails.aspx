﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageForDash.master" AutoEventWireup="true" CodeFile="Bankdetails.aspx.cs" Inherits="SprReports_Accounts_Bankdetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <br />
    <br />
    <section class="">

        
        
		<div class="container">
         
			<div class="row">
				<div class="col-md-5 col-sm-5">
                   
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="../../Images/Bank/SBILogo_state-bank-of-india-new.png" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">STATE BANK OF INDIA</h4>
						
                    <p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BENEFICIARY NAME : TRIPS KING<br />BANK NAME        :	STATE BANK OF INDIA<br />ACCOUNT NO       :	39176343813<br />IFSC code        :	SBIN0003208</p>
        
                          
						</div>
					</div>
                      
				</div>
			    <div class="col-md-5 col-sm-5">                                   
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="../../Images/Bank/ICICI-Bank-PNG-Icon-715x715.png" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">ICICI Bank</h4>
							<p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BENEFICIARY NAME :	TRIPS KING<br />BANK NAME        :	ICICI Bank<br />ACCOUNT NO       :	051405001938<br />IFSC code        :			ICIC0000514</p>
                           
                            
						</div>
					</div>
                      
				</div>
				<div class="col-md-3 col-sm-3">
                                            
					<img src ="../../Images/Bank/new_payment.png" style="width:100%;height:400px;"/>
                     
				</div>					
			</div>
            <hr />          
		</div>      
     <br />
     <br />
     <br />       

	</section>   

</asp:Content>

