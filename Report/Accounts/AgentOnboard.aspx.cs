﻿using DMT;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

public partial class SprReports_Accounts_AgentOnboard : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    PayGateReq Fetch = new PayGateReq();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == "" || Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        try
        {
            if (!IsPostBack)
            {

                //ddl_state.AppendDataBoundItems = True;
                ddl_state.Items.Clear();
                //ddl_state.Items.Insert(0, "Select State")
                ddl_state.DataSource = GETCITYSTATE("India", "COUNTRY");
                ddl_state.DataTextField = "STATE";
                ddl_state.DataValueField = "STATE";
                ddl_state.DataBind();

            }

        }
        catch (Exception ex)
        { }

    }
    protected void btnOnboard_Click(object sender, EventArgs e)
    {
        try
        {
            string Initiatorid = "";
            string errorMsg="";

            string panresponse = "";
            if (!string.IsNullOrEmpty(txtpannumber.Text.ToString()))
            {
                Initiatorid = ConfigurationManager.AppSettings["Initatior_Id"];
                //active pan card//
                //string panbody = "service_code=4&initiator_id=" + Initiatorid + "&user_code=85310001";
                //string panactivate = Fetch.GetCustomer(serviceUrl.panactivate, panbody, "PUT", "pancativate", Convert.ToInt64(txtmobileno.Text)); ;
                //////////////////
                
                ///string body = "pan_number=" + txtpannumber.Text.ToString().Trim() + "&purpose=1&initiator_id=" + Initiatorid + "&purpose_desc=onboarding&customer_mobile=" + txtmobileno.Text.ToString().Trim() + "";
               // panresponse = Fetch.GetCustomer(serviceUrl.panverify, body, "POST", "panverification", Convert.ToInt64(txtmobileno.Text));
                //if (!string.IsNullOrEmpty(panresponse))
                //{
                   // if (panresponse.ToLower().Contains("successful"))
                    //{
                        string dob = Txt_AdtDOB.Text;
                        conn.Open();
                        SqlCommand cmd = new SqlCommand("SP_InsertAgentOnboardDMT", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Initiatorid", Initiatorid);
                        cmd.Parameters.AddWithValue("@fname", txtfname.Text.ToUpper().Trim());
                        cmd.Parameters.AddWithValue("@lname", txtlname.Text.ToUpper().Trim());
                        cmd.Parameters.AddWithValue("@email", txtemail.Text.Trim());
                        cmd.Parameters.AddWithValue("@pannumber", txtpannumber.Text.ToUpper().Trim());
                        cmd.Parameters.AddWithValue("@mobilenumber", txtmobileno.Text.Trim());
                        cmd.Parameters.AddWithValue("@state", ddl_state.SelectedValue.ToUpper().Trim());
                        cmd.Parameters.AddWithValue("@city", txtcity.Text.ToUpper().Trim());
                        cmd.Parameters.AddWithValue("@district", txtDistrict.Text.ToUpper().Trim());
                        cmd.Parameters.AddWithValue("@area", txtarea.Text.ToUpper().Trim());
                        cmd.Parameters.AddWithValue("@pincode", txtpincode.Text.Trim());
                        cmd.Parameters.AddWithValue("@dob", dob);
                        cmd.Parameters.AddWithValue("@shopname", txtshopname.Text.ToUpper().Trim());
                        cmd.Parameters.AddWithValue("@Userid", Session["UID"].ToString().ToUpper());
                        cmd.Parameters.Add("@errorMsg", SqlDbType.Char, 500);
                        cmd.Parameters["@errorMsg"].Direction = ParameterDirection.Output;
                        


                    
                        int a = cmd.ExecuteNonQuery();
                        errorMsg = (string)cmd.Parameters["@errorMsg"].Value;
                        conn.Close();
                        if (a == 1 || errorMsg.Contains("already exists"))
                        {
                            string body_onboard = "";
                            body_onboard = "initiator_id=" + Initiatorid + "&pan_number=" + txtpannumber.Text.Trim() + "&mobile=" + txtmobileno.Text.Trim() + "&first_name=" + txtfname.Text.Trim() + "&last_name=" + txtlname.Text.Trim() + "&email=" + txtemail.Text.Trim() + "&residence_address={\"line\":\"Eko India\",\"city\":\"" + txtcity.Text.Trim() + "\",\"state\":\"" + ddl_state.SelectedValue + "\",\"pincode\":\"" + txtpincode.Text.Trim() + "\",\"district\":\"" + txtDistrict.Text.Trim() + "\",\"area\":\"" + txtarea.Text.Trim() + "\"}&dob=" + dob + "&shop_name=" + txtshopname.Text.ToString().Trim() + "";
                            string onboardresponse = Fetch.GetCustomer(serviceUrl.OnboardAgent, body_onboard, "PUT", "Onboardagent", Convert.ToInt64(txtmobileno.Text));
                            getusercode objgetusercode = new getusercode();
                            objgetusercode = JsonConvert.DeserializeObject<getusercode>(onboardresponse);
                            if (onboardresponse.Contains("Pan Number already exists"))
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('" + objgetusercode.message + "');", true);
                            }
                            if (onboardresponse.ToLower().Contains("successful") || onboardresponse.Contains("This user already exits"))
                            {
                                updateUserid(txtmobileno.Text.Trim(), objgetusercode.data.user_code);
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Agency Onboard Successfully');", true);
                                responsemsg.Text = objgetusercode.message;
                                Response.Redirect("~/Report/Accounts/UploadPrice.aspx");
                            }
                            else { ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('" + objgetusercode.message + "');", true); }
                        }

                //    }
                //    else { ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Pan number is not valid.');", true); }
                //}
            }

        }
        catch (Exception ex)
        { LogWriter.ErrorLog(ex.Message.ToString(), "btnOnboard_Click", Convert.ToInt64(txtmobileno.Text)); }
    }

    public void updateUserid(string mobilenumber,string Usercode)
    {
        conn.Open();
        SqlCommand cmd = new SqlCommand("Sp_UpdateAgentOnboardDMT", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@Mobilenumber", mobilenumber);
        cmd.Parameters.AddWithValue("@Usercode", Usercode);
        int a = cmd.ExecuteNonQuery();
        conn.Close();
    }

    protected void btnregister_Click(object sender, EventArgs e)
    {
        try
        {
            txtregistermobile.ReadOnly = true;
            string mobilenumber = txtregistermobile.Text;
            string otp = txtotp.Text;
            string registerbody = "";
            string Initiatorid = ConfigurationManager.AppSettings["Initatior_Id"];
            registerbody = "initiator_id=" + Initiatorid + "&mobile=" + mobilenumber + "&otp=" + otp + "";
            string verfiyuserresponse = Fetch.GetCustomer(serviceUrl.verifyuser, registerbody, "PUT", "btnregister_Click", Convert.ToInt64(mobilenumber));
            if (!string.IsNullOrEmpty(verfiyuserresponse))
            {
                verfiyuser objverfiyuser = new verfiyuser();
                objverfiyuser = JsonConvert.DeserializeObject<verfiyuser>(verfiyuserresponse);
                if (objverfiyuser.status == 0)
                {
                    divotp.Style.Add("display", "none");
                    divreg.Style.Add("display", "none");
                    divgetotp.Style.Add("display", "none");
                    divmobile.Style.Add("display", "none");
                    divonboardagent.Style.Add("display", "block");
                    txtmobileno.Text = mobilenumber;
                    
                }
            }
        }
        catch(Exception ex)
        {}
    }
    protected void btnrequestotp_Click(object sender, EventArgs e)
    {
        try
        {
            string mobilenumber = txtregistermobile.Text;
            string otpbody = "";
            string Initiatorid = ConfigurationManager.AppSettings["Initatior_Id"];
            otpbody = "initiator_id=" + Initiatorid + "&mobile=" + mobilenumber + "";
            string onboardresponse = Fetch.GetCustomer(serviceUrl.otpurl, otpbody, "PUT", "btnrequestotp_Click", Convert.ToInt64(mobilenumber));
            if (onboardresponse.Contains("User Already verified"))
            {
                divonboardagent.Style.Add("display", "block");
                divotp.Style.Add("display", "none");
                divreg.Style.Add("display", "none");
                divgetotp.Style.Add("display", "none");
                divmobile.Style.Add("display", "none");
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('User Already verified.');", true);
                txtmobileno.Text = mobilenumber;
                txtmobileno.ReadOnly = true;
            }
            else if (!string.IsNullOrEmpty(onboardresponse))
            {
                GETOTP objGETOTP = new GETOTP();
                objGETOTP = JsonConvert.DeserializeObject<GETOTP>(onboardresponse);
                if (objGETOTP.status == 0)
                {

                    divotp.Style.Add("display", "block");
                    divreg.Style.Add("display", "block");
                    divgetotp.Style.Add("display", "none");
                    txtregistermobile.ReadOnly = true;
                }
            }
        }
        catch (Exception ex)
        { }
    }

    protected DataTable GETCITYSTATE(string INPUT, string SEARCH)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        DataTable dt = new DataTable();
        SqlDataAdapter adap;
        adap = new SqlDataAdapter("SP_GET_STATECITY", con);
        adap.SelectCommand.CommandType = CommandType.StoredProcedure;
        adap.SelectCommand.Parameters.AddWithValue("@INPUT", INPUT);
        adap.SelectCommand.Parameters.AddWithValue("@SEARCH", SEARCH);

        adap.Fill(dt);

        return dt;
    }
    
    //UserCode Serialise//
    public class Data
    {
        public string user_code { get; set; }
        public string initiator_id { get; set; }
    }

    public class getusercode
    {
        public int response_status_id { get; set; }
        public Data data { get; set; }
        public int response_type_id { get; set; }
        public string message { get; set; }
        public int status { get; set; }
    }

    ////otp response//////
    public class otpstatus
    {
        public string initiator_id { get; set; }
        public string mobile { get; set; }
        public string otp { get; set; }
    }

    public class GETOTP
    {
        public int response_status_id { get; set; }
        public otpstatus data { get; set; }
        public int response_type_id { get; set; }
        public string message { get; set; }
        public int status { get; set; }
    }

    ////verify response//////

    public class verifydata
    {
        public string csp_id { get; set; }
    }

    public class verfiyuser
    {
        public int response_status_id { get; set; }
        public verifydata data { get; set; }
        public int response_type_id { get; set; }
        public string message { get; set; }
        public int status { get; set; }
    }
}