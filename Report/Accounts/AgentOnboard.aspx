﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterForHome.master" AutoEventWireup="true" CodeFile="AgentOnboard.aspx.cs" Inherits="SprReports_Accounts_AgentOnboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<link type="text/css" href="/Styles/jquery-ui-1.8.8.custom.css" rel="stylesheet">


    <div class="page-title-container">
        <div class="container">
            <div class="page-title pull-left">
                <h2 class="entry-title">Agent Onboard</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2" id="divmobile" runat="server">
            <asp:TextBox runat="server" ID="txtregistermobile" CssClass="form-control full-width1" placeholder="MobileNo" onkeypress='validate(event)' MaxLength="10"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqtxtregistermobile" ControlToValidate="txtregistermobile" ValidationGroup="getotp" runat="server" ErrorMessage="Required Mobile Number"></asp:RequiredFieldValidator>
        </div>
        <div class="col-md-2" style="display:none;" id="divotp" runat="server">
            <asp:TextBox runat="server" ID="txtotp" CssClass="form-control full-width1" placeholder="OTP" onkeypress='validate(event)' MaxLength="3"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqtxtotp" ControlToValidate="txtotp" ValidationGroup="regmobile" runat="server" ErrorMessage="Required otp" ></asp:RequiredFieldValidator>
        </div>
        <div class="col-md-2" id="divgetotp" runat="server">
            <asp:Button ID="btnrequestotp" runat="server" OnClick="btnrequestotp_Click" class="btn btn-success" Text="GET OTP" ValidationGroup="getotp" />
        </div>
        <div class="col-md-2" style="display:none;" id="divreg" runat="server">
            <asp:Button ID="btnregister" runat="server" OnClick="btnregister_Click" class="btn btn-success" Text="Submit" ValidationGroup="regmobile" />
        </div>
    </div>
    <div id="divonboardagent" runat="server" style="display:none;">
    <div class="row">
        <div class="col-md-2">
            <div class="input-group">
                <asp:TextBox runat="server" ID="txtfname" CssClass="form-control full-width1" placeholder="First Name" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqtxtfname" ControlToValidate="txtfname" ValidationGroup="VaildateOnboard" runat="server" ErrorMessage="Required First Name"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="col-md-2">
            <div class="input-group">
                <asp:TextBox runat="server" ID="txtlname" CssClass="form-control full-width1" placeholder="Last Name" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqtxtlname" ControlToValidate="txtlname" ValidationGroup="VaildateOnboard" runat="server" ErrorMessage="Required Last Name"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="col-md-2">
            <div class="input-group">
                <asp:TextBox runat="server" ID="txtemail" CssClass="form-control full-width1" placeholder="Email" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqtxtemail" ControlToValidate="txtemail" ValidationGroup="VaildateOnboard" runat="server" ErrorMessage="Required Email ID"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">&nbsp;</div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <div class="input-group">
                <asp:TextBox runat="server" ID="txtpannumber" CssClass="form-control full-width1" placeholder="Pan number"  MaxLength="10"></asp:TextBox>
               <asp:RequiredFieldValidator ID="reqtxtpannumber" ControlToValidate="txtpannumber" ValidationGroup="VaildateOnboard" runat="server" ErrorMessage="Required Pan Number"></asp:RequiredFieldValidator>

            </div>
        </div>
        <div class="col-md-2">
            <div class="input-group">
                <asp:TextBox runat="server" ID="txtmobileno" CssClass="form-control full-width1" placeholder="Mobile Number" onkeypress='validate(event)' ReadOnly="true"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqtxtmobileno" ControlToValidate="txtmobileno" ValidationGroup="VaildateOnboard" runat="server" ErrorMessage="Required Mobile Number"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="col-md-3">
            <div class="input-group">
                               <asp:DropDownList ID="ddl_state" runat="server" AutoPostBack="True" CssClass="input-text full-width">
                                </asp:DropDownList>
                <%--<asp:TextBox runat="server" ID="txtstate" CssClass="form-control full-width1" placeholder="State" ></asp:TextBox>--%>
                <%--<asp:RequiredFieldValidator ID="reqtxtstate" ControlToValidate="txtstate" ValidationGroup="VaildateOnboard" runat="server" ErrorMessage="Required State"></asp:RequiredFieldValidator>--%>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">&nbsp;</div>
    </div>

    <div class="row">
        <div class="col-md-2">
            <div class="input-group">
                <asp:TextBox runat="server" ID="txtcity" CssClass="form-control full-width1" placeholder="City" ></asp:TextBox>
<asp:RequiredFieldValidator ID="reqtxtcity" ControlToValidate="txtcity" ValidationGroup="VaildateOnboard" runat="server" ErrorMessage="Required City"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="col-md-2">
            <div class="input-group">
                <asp:TextBox runat="server" ID="txtDistrict" CssClass="form-control full-width1" placeholder="District" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqtxtDistrict" ControlToValidate="txtDistrict" ValidationGroup="VaildateOnboard" runat="server" ErrorMessage="Required District"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="col-md-2">
            <div class="input-group">
                <asp:TextBox runat="server" ID="txtarea" CssClass="form-control full-width1" placeholder="Area" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqtxtarea" ControlToValidate="txtarea" ValidationGroup="VaildateOnboard" runat="server" ErrorMessage="Required Area"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">&nbsp;</div>
    </div>
    <div class="row">

        <div class="col-md-2">
            <div class="input-group">
                <asp:TextBox runat="server" ID="txtpincode" CssClass="form-control full-width1" placeholder="Pincode" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqtxtpincode" ControlToValidate="txtpincode" ValidationGroup="VaildateOnboard" runat="server" ErrorMessage="Required Pincode"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="col-md-2">
            <div class="input-group">
                <asp:TextBox CssClass="adtdobcss form-control full-width1" ID="Txt_AdtDOB" runat="server" placeholder="DOB" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqTxt_AdtDOB" ControlToValidate="Txt_AdtDOB" ValidationGroup="VaildateOnboard" runat="server" ErrorMessage="Required Date of Birth"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="col-md-2">
            <div class="input-group">
                <asp:TextBox runat="server" ID="txtshopname" CssClass="form-control full-width1" placeholder="ShopName" ></asp:TextBox>
                
                <asp:RequiredFieldValidator ID="reqtxtshopname" ControlToValidate="txtshopname" ValidationGroup="VaildateOnboard" runat="server" ErrorMessage="Required Shopname"  ValidationExpression="^[0-9a-zA-Z]{1,4}$"></asp:RequiredFieldValidator>

            </div>
        </div>
    </div>

    <div class="row"> 
        <div class="col-md-2">&nbsp;</div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <asp:Label runat="server" ID="responsemsg"></asp:Label>&nbsp;
        </div>
        <div class="col-md-2">&nbsp;</div>
        <div class="col-md-2">
            <asp:Button ID="btnAdd" runat="server" OnClick="btnOnboard_Click" class="btn btn-success" Text="Submit" ValidationGroup="VaildateOnboard" />
        </div>
    </div>
        </div>
    <script type="text/jscript">
        function validate(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

    </script>



    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript">
        var d = new Date();

        $(function () { var d = new Date(); var dd = new Date(1952, 01, 01); $(".adtdobcss").datepicker({ numberOfMonths: 1, dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true, yearRange: ('1920:' + (d.getFullYear() - 12) + ''), navigationAsDateFormat: true, showOtherMonths: true, selectOtherMonths: true, defaultDate: dd }) });



        $('#ctl00_ContentPlaceHolder1_txtshopname').bind('keypress', function (e) {
            debugger;

            if ($('#ctl00_ContentPlaceHolder1_txtshopname').val().length == 0) {
                var k = e.which;
                var ok = k >= 65 && k <= 90 || // A-Z
            k >= 97 && k <= 122 || // a-z
            k >= 48 && k <= 57; // 0-9

                if (!ok) {
                    e.preventDefault();
                }
            }
        }); 
    </script>

    <%--    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />--%>
</asp:Content>

