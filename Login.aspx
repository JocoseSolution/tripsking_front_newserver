﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="false" EnableEventValidation="false"
    CodeFile="Login.aspx.vb" Inherits="Login" %>

<%@ Register Src="~/UserControl/LoginControl.ascx" TagPrefix="UC1" TagName="Login" %>
<%@ Register Src="~/UserControl/IssueTrack.ascx" TagName="Holiday" TagPrefix="ucholiday" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <UC1:Login ID="uc" runat="server" />
    <style type="text/css">
        table {
            width: 100% !important;
        }
    </style>
    <div class="toPopup1" style="display: none;">
        <div style="width: 32%; padding: 1%; margin: 50px auto 0; background: #f9f9f9;">
            <div style="font-weight: bold;">
                Timings 10 AM to 6 PM, Monday to Saturday. Issues Reported after 6 PM and on Sunday will be addressed on the following day.
            </div>
            <div>
                <ucholiday:Holiday ID="uc_holiday" runat="server" />
            </div>
        </div>
    </div>

    <style type="text/css">
        .mySlides {
            display: none;
        }

            /*.mySlides img {
                vertical-align: middle;
                width: 100% !important;
            }*/
        /* Slideshow container */
        .slideshow-container {
            max-width: 1000px;
            position: relative;
            margin: auto;
        }

        /* Next & previous buttons */
        .prev, .next {
            cursor: pointer;
            position: absolute;
            top: 50%;
            width: auto;
            padding: 5px;
            margin-top: -22px;
            color: white;
            font-weight: bold;
            font-size: 18px;
            transition: 0.6s ease;
            border-radius: 0 3px 3px 0;
            user-select: none;
        }

        /* Position the "next button" to the right */
        .next {
            right: 0;
            border-radius: 3px 0 0 3px;
        }

            /* On hover, add a black background color with a little bit see-through */
            .prev:hover, .next:hover {
                background-color: #ffc000;
                color: #fff !important;
                text-decoration: none !important;
            }

        /* Caption text */
        .text {
            color: #f2f2f2;
            font-size: 15px;
            padding: 8px 12px;
            position: absolute;
            bottom: 8px;
            width: 100%;
            text-align: center;
        }

        /* Number text (1/3 etc) */
        .numbertext {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }

        .fade {
            -webkit-animation-name: fade;
            -webkit-animation-duration: 1.5s;
            animation-name: fade;
            animation-duration: 1.5s;
        }

        @-webkit-keyframes fade {
            from {
                opacity: .4;
            }

            to {
                opacity: 1;
            }
        }

        @keyframes fade {
            from {
                opacity: .4;
            }

            to {
                opacity: 1;
            }
        }

        /* On smaller screens, decrease text size */
        @media only screen and (max-width: 300px) {
            .prev, .next, .text {
                font-size: 11px;
            }
        }
    </style>
    <%If NotificationContentCount > 0 Then%>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document" style="margin-top: 7px;">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute;top: -1%;right: -1%;z-index: 99999;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                <%--<div class="modal-header" style="background: #fff!important">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>--%>
                <div class="modal-body">
                    <div class="slideshow-container">
                        <%=NotificationContent %>
                        <%If NotificationContentCount > 1 Then%>
                        <a class="prev" onclick="plusSlides(-1)" style="background: #ffc000; margin-left: -15px;">&#10094;</a>
                        <a class="next" onclick="plusSlides(1)" style="background: #ffc000; margin-right: -15px;">&#10095;</a>
                        <%End If%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>
    <script type="text/javascript">
        $(window).load(function () { $('#exampleModal').modal('show'); });

        var slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
            showSlides(slideIndex += n);
        }
        function showSlides(n) {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("dot");
            if (n > slides.length) { slideIndex = 1 }
            if (n < 1) { slideIndex = slides.length }
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex - 1].style.display = "block";
            dots[slideIndex - 1].className += " active";
        }
    </script>
    <%End If%>
</asp:Content>
