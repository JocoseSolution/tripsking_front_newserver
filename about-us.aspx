﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="false" CodeFile="about-us.aspx.vb" Inherits="about_us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link href="Advance_CSS/css/bootstrap.css" rel="stylesheet" />
    <link href="Advance_CSS/css/styles.css" rel="stylesheet" />
    
     <div class="theme-hero-area theme-hero-area-half">
        <div class="theme-hero-area-bg-wrap">
        
            <div class="theme-hero-area-bg" style="background-image: url(Advance_CSS/Images/about.jpg);"></div>
            <div class="theme-hero-area-mask theme-hero-area-mask-half"></div>
            <div class="theme-hero-area-inner-shadow"></div>
        </div>
        <div class="theme-hero-area-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 theme-page-header-abs">
                        <div class="theme-page-header theme-page-header-lg">
                            <h1 class="theme-page-header-title">About us</h1>
                            <p class="theme-page-header-subtitle">The Story of Our Company</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="theme-page-section theme-page-section-xl theme-page-section-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-12">                  
                                    <div class="theme-about-us-section-body">
                                        <p>Trips King is India’s leading online travel company. Positioned as a brand that believes in "Creating Happy Travellers", we provide information, pricing, availability, and booking facility for domestic and international air travel, hotel bookings, holiday packages and bus and railway reservations. We offer a host of travel services designed to make business and leisure travel easier.</p>
                                        <p>Based in Moradabad , Uttar Pradesh, India, Tripsking.in is a one-stop-shop for all travel-related services. A leading consolidator of travel products, Trips King provides reservation facility for more than 12,000 hotels in India and over 400,000 hotels around the world. Through continued excellence in providing travel solutions, responses to booking travel online through Trips King have also reached new heights with the company doing 20,000 domestic tickets and 5000 hotels and holiday packages a day.</p>
                                       <p>Customers can access Trips King through multiple ways: through our user-friendly website, mobile optimised WAP site and applications, 24x7 multi-lingual call center, a countrywide network of Holiday Lounges and Yatra Travel Express stores. Trips King provides booking facility for all the popular as well as exotic national and international destinations. Launched in July 2013, Trips King is today ranked as the leading provider of consumer-direct travel services in India. Trips King has emerged as the most trusted travel brand in India.</p>
                                    </div>                              
                            </div>
                        </div>                  
                              
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

